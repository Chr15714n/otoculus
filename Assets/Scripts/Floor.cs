﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Floor : MonoBehaviour {

    [BoxGroup("Player Info")]
    public int myPlayerNumber;
    [BoxGroup("Player Info")]
    public PlayerManager myPlayer;
    [BoxGroup("Puzzle Stuff")]
    public BeatPointManager myBeatPoint;
    [BoxGroup("Player Info")]
    public Net myNet;
    [BoxGroup("Player Info")]
    public Ball ball;
    [BoxGroup("Components")]
    public Animator anim;
    [BoxGroup("Components")]
    public Conductor conductor;
    [BoxGroup("Components")]
    public GameManager gm;
    [BoxGroup("Puzzle Stuff")]
    public MeshRenderer[] beatPointMeshes;
    [BoxGroup("Puzzle Stuff")]
    public GameObject puzzlePrefab;
    [BoxGroup("Puzzle Stuff")]
    public Vector3 _BPLoc;
    [BoxGroup("Puzzle Stuff")]
    public Vector3 _AdjustedLoc;

    [BoxGroup("State Booleans")]
    public bool hitMe = false;
    [BoxGroup("State Booleans")]
    public bool hideMe = true;
    [BoxGroup("State Booleans")]
    public bool spawnTime = false;


	// Use this for initialization
	void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        ball = gm.ball;
        anim = GetComponent<Animator>();
        conductor = FindObjectOfType<Conductor>();
        myBeatPoint = GetComponentInChildren<BeatPointManager>();
        beatPointMeshes = myBeatPoint.gameObject.GetComponentsInChildren<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

        if (myPlayer == null) return;
        //{
            //if (gm.players.ToArray().Length <)


        //} 

        Vector3 playerForward = myPlayer.transform.forward;

        if (myBeatPoint != null)
        {
            if (conductor.currentRoundName.Contains("PowerUp"))
            {
                hitMe = true;
            }
            else
            {
                hideMe = true;
            }

            if (hitMe)
            {
                anim.StartPlayback();
                foreach (MeshRenderer mesh in beatPointMeshes)
                {
                    mesh.enabled = true;
                    _BPLoc = myBeatPoint.transform.position;
                }
                hitMe = false;
            }

            if (hideMe)
            {
                anim.StopPlayback();
                foreach (MeshRenderer mesh in beatPointMeshes)
                {
                    mesh.enabled = false;
                }
                hideMe = false;
            }            
        }

        if (spawnTime)
        {
            Instantiate(puzzlePrefab, _BPLoc, Quaternion.identity);
            spawnTime = false;
        }
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<PlayerManager>() && myPlayer == null)
        {
            myPlayer = other.GetComponent<PlayerManager>();
        }

        if (other.GetComponent<BeatPointManager>() && myBeatPoint == null)
        {
            myBeatPoint = other.GetComponentInChildren<BeatPointManager>();
        }

        if (other.GetComponent<Net>() && myNet == null)
        {
            myNet = other.GetComponent<Net>();
        }
    }
}
