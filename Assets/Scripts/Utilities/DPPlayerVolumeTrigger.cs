﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DPPlayerVolumeTrigger : MonoBehaviour {

    public DBTriggerComponent _volumeTrigger;
    public List<DBPlayerNetworkComponent> _playersInTrigger;

	// Use this for initialization
	void Start () {
        _playersInTrigger = new List<DBPlayerNetworkComponent>();
        

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {

            //Debug.Log("Player: " + other.GetComponentInChildren<DBPlayerNetworkComponent>()._motiveReferenceID);
            if(!_playersInTrigger.Contains(other.GetComponentInParent<DBPlayerNetworkComponent>()) && other.tag.Equals("PlayerBody"))
                _playersInTrigger.Add(other.GetComponentInParent<DBPlayerNetworkComponent>());

    }

    private void OnTriggerExit(Collider other)
    {
        
            //Debug.Log("Player: " + other.GetComponentInChildren<DBPlayerNetworkComponent>()._motiveReferenceID);
            if (_playersInTrigger.Contains(other.GetComponentInParent<DBPlayerNetworkComponent>()))
                _playersInTrigger.Remove(other.GetComponentInParent<DBPlayerNetworkComponent>());

    }
}
