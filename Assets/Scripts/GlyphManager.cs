﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GlyphManager : NetworkBehaviour {

    public DBBall ball;
    public DBTeamInfo.TeamID team;
    public Component[] nodes;
    public int nodePower = 0;
    public float BPM;
    public float Beat = 0;
    public float Bar = 0;
    public float Measure = 0;
    public float ActiveNodes = 0;
    public GameObject resetButton;
    public bool _pendingDestroy = false;

    public DBMusicConductor _conductor;

	// Use this for initialization
	void Start () {
        //ball = GameObject.Find("Ball");
        resetButton = GameObject.Find("ManualReset");       
        nodes = GetComponentsInChildren<Node>();
        StartCoroutine("PuzzlePlay");
        _conductor = GameObject.Find("MusicConductor").GetComponent<DBMusicConductor>();
    }
	
	// Update is called once per frame
	void Update ()
    {

        Beat = _conductor.currentBeats-1;
        //RpcBeatUpdate();

        if (Bar == 4)
        {
            Measure++;
            Bar = 0;
            _pendingDestroy = true;
            //Destroy(gameObject);
        }

        /*

        if (Beat == 4)
        {
            if (nodePower == nodes.Length)
            {
                StopCoroutine("PuzzlePlay");
                //ball.GetComponent<Ball>().isPoweredUp = true;
                _pendingDestroy = true;
                //Destroy(gameObject);
            } else
            {
                ActiveNodes = 0;                
            }                              
        }
        */

        if (Measure == 4)
        {
            Measure = 0;
        }

        //if (resetButton.GetComponent<Reset>().ResetMe)
        //{
        //    resetButton.GetComponent<Reset>().ResetMe = false;
        //    ball.GetComponent<Ball>().isPoweredUp = false;
        //    ActiveNodes = 0;
        //    Bar = 0;
        //    Beat = 0;
        //    nodePower = 0;
        //    GetComponent<AudioSource>().enabled = false;
        //    StartCoroutine("PuzzlePlay");
        //}
    }

    private IEnumerator PuzzlePlay() {
        while (true)
        {     
            
            nodePower = 0;
            NodePowerUp();
            NodePowerDown();
            
            yield return Beat == 1;

            NodePowerUp();
            NodePowerDown();
            
            yield return Beat == 2;
            NodePowerUp();
            NodePowerDown();
            
            yield return Beat == 3;
            NodePowerUp();
            NodePowerDown();

            //Bar++;

            //yield return Beat == 4;
            //NodePowerDown();
            
            //Bar++;
        }
    }

    private void NodePowerUp()
    {
        foreach (Node node in nodes)
        {
            if (Beat == node.myNodeNr)
            {
                node.GetComponent<Collider>().enabled = true;
                node.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
            }
        }
    }
    private void NodePowerDown()
    {
        foreach (Node node in nodes)
        {
            if (Beat != node.myNodeNr && node.myNodeNr >= ActiveNodes)
            {
                node.GetComponent<Collider>().enabled = false;
                node.GetComponent<Renderer>().material.SetColor("_EmissionColor", node.myColor);
            }
        }
    }

   

}
