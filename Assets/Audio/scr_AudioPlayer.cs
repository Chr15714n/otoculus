﻿using UnityEngine;
using System.Collections;

public class scr_AudioPlayer : MonoBehaviour {

    //tempo timer stuff
    public int beatsPerBar;
    public bool tick;
    float timePerBar;
    float timePerBeat;
    public float clipTime;

    public AudioClip[] clipParts;
    public AudioClip nextClipPart;

    AudioSource DrumAmp;

    // public float LeadTime;

    // Use this for initialization
    void Start () {
        DrumAmp = GetComponent<AudioSource>();

        SetupDrums();
      

        //clipTime = DrumAmp.clip.length;

        timePerBar = clipTime;
         
    }
	
	// Update is called once per frame
	void Update () {
        clipTime -= Time.deltaTime;
        timePerBeat -= Time.deltaTime;
       
        //setup beat counter
        if (timePerBeat<= 0)
        {
            tick = !tick;
            timePerBeat = timePerBar / beatsPerBar;
        }

        //setup and play next drum track
        if (clipTime <= 0)
        {
            DrumAmp.clip = nextClipPart;
            clipTime = DrumAmp.clip.length;
            

            timePerBar = clipTime;
            timePerBeat = timePerBar / beatsPerBar;

            DrumAmp.Play();

            //metronome
            tick = !tick;
            
            //roll for next part
            SetupDrums();
        }
       
    }

    void SetupDrums()
    {
        int random1 = Mathf.FloorToInt(Random.value * clipParts.Length);
        nextClipPart = clipParts[random1];
    }
    
}
