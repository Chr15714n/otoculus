﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class Conductor : MonoBehaviour {

    //public double sampleRate = 0.0f;
    //public double nextTick = 0.0f;
    // public double startTick = 0.0f;

    [BoxGroup("BPM")]
    public string currentRoundName;
    [BoxGroup("BPM")]
    public float BPM;
    float timeMod;
    [BoxGroup("Timing Info")]
    public int totalBeats;
    [BoxGroup("Timing Info")]
    public int currentBeats;
    [BoxGroup("Timing Info")]
    public int totalBars=1;
    [BoxGroup("Timing Info")]
    public int currentRoundBars=1;
    [BoxGroup("Timing Info")]
    public bool ticker = false;
    [HideInInspector]
    public int currentRound = 0;


    [FoldoutGroup("Round Information")]
    public string[] roundNames;
    [FoldoutGroup("Round Information")]
    public int[] roundLengths;
    [HideInInspector]
    public bool go = false;

    // Use this for initialization
    void Start () {
        //double startTick = AudioSettings.dspTime;
       // sampleRate = AudioSettings.outputSampleRate;
       // nextTick = startTick * sampleRate;


        timeMod =BPM /60;
        currentRoundName = roundNames[currentRound];
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (!go)
            {
                go = true;
                GetComponent<AudioSource>().Play();
            }            
        }
        if (!go) return;




        //transform.position += new Vector3(timeMod * Time.deltaTime, 0, 0);
        transform.parent.transform.eulerAngles += new Vector3(0, timeMod * 45 * Time.deltaTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentRound < roundNames.Length-1)
        {
            ticker = !ticker;
            totalBeats += 1;
            currentBeats += 1;

            if (currentBeats == 9)
            {
                currentBeats = 1;
                currentRoundBars += 1;
                totalBars += 1;

                if (currentRoundBars == roundLengths[currentRound] + 1)
                {
                    currentRoundBars = 1;
                    currentRound += 1;
                    currentRoundName = roundNames[currentRound];
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentRound < roundNames.Length - 1)
        {
            
            totalBeats += 1;
            currentBeats += 1;

            if (currentBeats == 9)
            {
                currentBeats = 1;
                currentRoundBars += 1;
                totalBars += 1;

                if (currentRoundBars == roundLengths[currentRound] + 1)
                {
                    currentRoundBars = 1;
                    currentRound += 1;
                    currentRoundName = roundNames[currentRound];
                }
            }
            //ticker = 0;
        }
       
    }
}
