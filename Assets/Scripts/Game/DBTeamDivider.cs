﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBTeamDivider : MonoBehaviour {

    public int _phase = 0;
    public GameObject _aPhase;
    public GameObject _bPhase;
    public GameObject _cPhase;

    public BoxCollider[] _aColliders;
    public BoxCollider[] _bColliders;

    // Use this for initialization
    void Start () {
        _aPhase = GameObject.Find("APhase");
        _bPhase = GameObject.Find("BPhase");
        _cPhase = GameObject.Find("CPhase");

        _aColliders = _aPhase.GetComponentsInChildren<BoxCollider>();
        _bColliders = _bPhase.GetComponentsInChildren<BoxCollider>();
    }
	
	// Update is called once per frame
	void Update () {
		if(_phase != 0)
        {
            if(_phase == 1)
            {
                foreach(BoxCollider collider in _aColliders)
                {
                    collider.enabled = true;
                }
                foreach(BoxCollider collider in _bColliders)
                {
                    collider.enabled = false;
                }
                _cPhase.GetComponentInChildren<BoxCollider>().enabled = false;
            } else if (_phase == 2)
            {
                foreach (BoxCollider collider in _aColliders)
                {
                    collider.enabled = false;
                }
                foreach (BoxCollider collider in _bColliders)
                {
                    collider.enabled = true;
                }
                _cPhase.GetComponentInChildren<BoxCollider>().enabled = false;
            } else if (_phase == 3)
            {
                foreach (BoxCollider collider in _aColliders)
                {
                    collider.enabled = false;
                }
                foreach (BoxCollider collider in _bColliders)
                {
                    collider.enabled = false;
                }
                _cPhase.GetComponentInChildren<BoxCollider>().enabled = true;
            } else
            {
                foreach (BoxCollider collider in _aColliders)
                {
                    collider.enabled = false;
                }
                foreach (BoxCollider collider in _bColliders)
                {
                    collider.enabled = false;
                }
                _cPhase.GetComponentInChildren<BoxCollider>().enabled = false;
            }
        }
	}
}
