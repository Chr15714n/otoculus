﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBGlyphNode : NetworkBehaviour
{
	public Color _poweredOnColor = Color.black;
	public Color _poweredOffColor = Color.white;
	public Color _triggeredColor = Color.red;

	Renderer _renderer;
	Material _material;
	Collider _collider;

	public DBGlyph GlyphParent
	{
		set; get;
	}

	public int _nodeID;

	private bool _poweredOn;
	public bool PoweredOn { get { return _poweredOn;  } }

    [SyncVar (hook = "OnNodeTriggered")]
	private bool _triggered;

	public bool Triggered { get { return _triggered; } }


	void Awake()
	{
		if (_renderer == null)
		{
			_renderer = GetComponent<Renderer>();
		}
		if (_material == null)
		{
			_material = _renderer.material;
		}
		if (_collider == null)
		{
			_collider = GetComponent<Collider>();
		}
	}

	void Start ()
	{
		//Debug.Log("GlyphNode Start");
		
	}

	public void ResetNode()
	{
		_poweredOn = false;
		_triggered = false;

		UpdateMaterial();
	}

	public void UpdateMaterial()
	{
		if(_triggered)
		{
			_material.SetColor("_EmissionColor", _triggeredColor);
		}
		else if(_poweredOn)
		{
			_material.SetColor("_EmissionColor", _poweredOnColor);
		}
		else
		{
			_material.SetColor("_EmissionColor", _poweredOffColor);
		}
	}

	public void PowerUp()
	{
		_collider.enabled = true;
		_poweredOn = true;
		UpdateMaterial();

		//Debug.Log("PowerUp: " + _nodeID);
	}

	public void PowerDown()
	{
		_collider.enabled = false;
		_poweredOn = false;
		UpdateMaterial();

		//Debug.Log("PowerDown: " + _nodeID);
	}

    void OnNodeTriggered(bool triggered)
    {
        Debug.Log("Updating Trigger");
        _triggered = triggered;
        UpdateMaterial();
    }

    public void TriggerIt()
	{
		_triggered = true;
		UpdateMaterial();
	}
    /*
	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("OnTriggerEnter: " + other.name);

		// If player triggers while powered on, then we got a hit
		if(_poweredOn && !Triggered)
		{
			if (other.tag == "Ball")
			{
				GlyphParent.NotifyNodeTriggered(this, other);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		
	}
    */

}
