﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Ball : MonoBehaviour {
    [BoxGroup("Charge Information")]
    public int myChargeLevel;

	// Use this for initialization
	void Start ()
    {
        myChargeLevel = 0;
    }

        // Update is called once per frame
    void Update()
    {
        LevelCheck();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<PlayerManager>())
        {                       
            myChargeLevel = 0;
        }

        if (col.gameObject.GetComponent<Hand>())
        {
            myChargeLevel++;            
        }

        if (col.gameObject.GetComponent<Net>())
        {
            myChargeLevel = 0;
        }

        if (col.gameObject.GetComponent<Floor>())
        {
            myChargeLevel = 0;
        }
    }

    private void LevelCheck()
    {
        if (myChargeLevel > 4)
        {
            myChargeLevel = 4;
        }

        if (myChargeLevel < 0)
        {
            myChargeLevel = 0;
        }
    }
}
