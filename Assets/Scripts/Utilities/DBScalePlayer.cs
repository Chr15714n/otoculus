﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DBScalePlayer : NetworkBehaviour
{



    [SyncVar(hook = "onPlayerResize")]
    public float scalevalue;
    public float temp;

    // Use this for initialization
    void Start()
    {
        scalevalue = 1.0f;
        temp = scalevalue;
        //RpcScalePlayer();
    }

    private void Update()
    {
        temp = scalevalue;
    }

    private void OnGUI()
    {
        if (isLocalPlayer)
        {
            scalevalue = GUI.HorizontalSlider(new Rect(Screen.width / 2, Screen.height / 2, 100, 30), scalevalue, 0.5f, 1.5f);
            transform.localScale = new Vector3(temp, temp, temp);
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 + 45, 100, 30), "Apply"))
            {
                if (isLocalPlayer)
                {
                    transform.localScale = new Vector3(scalevalue, scalevalue, scalevalue);
                }
                Debug.Log("Resize: " + scalevalue);
                CmdScalePlayer(scalevalue);
            }
        }

    }

    public void onPlayerResize(float newSize)
    {

        scalevalue = newSize;
        //transform.localScale = new Vector3(value, value, value);

    }

    [ClientRpc]
    public void RpcScalePlayer(float _scalevalue)
    {
        Debug.Log("RPC: Size: " + _scalevalue);
        transform.localScale = new Vector3(_scalevalue, _scalevalue, _scalevalue);
    }

    [Command]
    public void CmdScalePlayer(float _scalevalue)
    {
        RpcScalePlayer(_scalevalue);
        Debug.Log("Size: " + scalevalue);
        //transform.localScale = new Vector3(value, value, value);

    }

}


/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DBScalePlayer : NetworkBehaviour {

    [SyncVar(hook = "onPlayerResize")]
    public float scalevalue;
    public float temp;

	// Use this for initialization
	void Start () {
        scalevalue = 1.0f;
        temp = scalevalue;
	}

    private void Update()
    {
        transform.localScale = new Vector3(scalevalue, scalevalue, scalevalue);
    }

    private void OnGUI()
    {
        if (isLocalPlayer)
        {
            scalevalue = GUI.HorizontalSlider(new Rect(Screen.width / 2, Screen.height / 2, 100, 30), scalevalue, 0.5f, 1.5f);
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 2 + 45, 100, 30), "Apply"))
            {
                if (isLocalPlayer)
                {
                    transform.localScale = new Vector3(scalevalue, scalevalue, scalevalue);
                }
                Debug.Log("Resize: " + scalevalue);
                CmdScalePlayer(scalevalue);
            }
        }
    }

    public void onPlayerResize(float newSize)
    {
        scalevalue = newSize;
    }

    [ClientRpc]
    public void RpcScalePlayer(float _scalevalue)
    {
        Debug.Log("RPC: Size: " + _scalevalue);
        transform.localScale = new Vector3(_scalevalue, _scalevalue, _scalevalue);
    }

    [Command]
    public void CmdScalePlayer(float _scalevalue)
    {
        RpcScalePlayer(_scalevalue);
        Debug.Log("Size: " + scalevalue);
    }

}
*/
