﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;
using Vlad.Networking;
using Morgan.Networking;

public class NetworkHandsManager : NetworkBehaviour {

    public Transform _leftHand, _rightHand;

    /*
    public bool _rightHoldButton
    {
        get
        {
            if (VRDevice.isPresent)
                return OVRInput.Get(OVRInput.Button.One);
            else
                return Input.GetMouseButton(1);
        }

    }
    public bool _leftHoldButton
    {
        get
        {
            if (VRDevice.isPresent)
                return OVRInput.Get(OVRInput.Button.Three);
            else
                return Input.GetMouseButton(0);
        }

    }

    */
    public bool _rightHoldButton;
    public bool _leftHoldButton;


    public bool _leftHoldingBall = false, _rightHoldingBall = false;

    public bool LeftHold, RightHold;
    public Transform networkBall;
    [Range(1, 20)]
    public float DebugVelocity;

    
    public Vector3 handVelocity
    {
        get
        {
            if (VRDevice.isPresent)
                return OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
            else
                return Vector3.Normalize(transform.forward + transform.up) * DebugVelocity;
        }
    }
    
    
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update () {
        LeftHold = _leftHoldButton;
        RightHold = _rightHoldButton;
        if(networkBall == null)
        {
            networkBall = GameObject.Find("NetworkBall").transform;
            return;
        }
        //if (networkBall != null) { return; }
        if (networkBall.GetComponent<NetworkBallManager>().handHoldingMe != null
        && !_rightHoldingBall
        && networkBall.GetComponent<NetworkBallManager>().handHoldingMe == _rightHand.transform
        && _rightHoldButton)
        {
            _rightHoldingBall = true;
            networkBall.GetComponent<NetworkBallManager>().held = true;
            CmdParentRightHand();
        }
        else if(_rightHoldingBall
        && !_rightHoldButton)
        {
            _rightHoldingBall = false;
            networkBall.GetComponent<NetworkBallManager>().held = false;
            networkBall.parent = null;
            CmdParentRightHand();
        }

        if (networkBall.GetComponent<NetworkBallManager>().handHoldingMe != null
        && !_leftHoldingBall
        && networkBall.GetComponent<NetworkBallManager>().handHoldingMe == _leftHand.transform
        && _leftHoldButton)
        {
            _leftHoldingBall = true;
            networkBall.GetComponent<NetworkBallManager>().held = true;
            CmdParentLeftHand();
        }
        else if (_leftHoldingBall
        && !_leftHoldButton)
        {
            _leftHoldingBall = false;
            networkBall.GetComponent<NetworkBallManager>().held = false;
            networkBall.parent = null;
            CmdParentLeftHand();
        }

    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        if (!GetComponentInParent<NetworkIdentity>().isLocalPlayer)
        {
            //this.GetComponent<CapsuleCollider>().enabled = false;
            //this is the essential of it. set enabled to false will prevent the Update function from being called.
            this.enabled = false;
        }
        networkBall = GameObject.Find("NetworkBall").transform;
    }

    //void NetworkGrab()
    //{
    //    Debug.Log("GRABBBB you bitch");
    //    networkBall.GetComponent<NetworkBallManager>().RpcBallGrabbed();
    //}


    //void NetworkRelease()
    //{
    //    networkBall.GetComponent<NetworkBallManager>().RpcBallThrown();
    //}


    public void BallHolderUpdate(GameObject trans)
    {

        networkBall.GetComponent<NetworkBallManager>().handHoldingMe = trans.transform;

    }

    [Command]
    public void CmdParentRightHand() {
        networkBall.GetComponent<NetworkBallManager>().onHandHoldingChanged(_rightHand);
    }

    [Command]
    public void CmdParentLeftHand() {
        networkBall.GetComponent<NetworkBallManager>().onHandHoldingChanged(_leftHand);
    }

    [Command]
    public void CmdUnparent() {
        networkBall.GetComponent<NetworkBallManager>().onHandHoldingChanged(null);
    }
}
