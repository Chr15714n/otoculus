﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Needs to be reworked with State Machine
public class DBNetworkUI : MonoBehaviour {

    public bool hidden = false;

    public bool usingOptitrack = false;

    public OptitrackStreamingClient _osc;

    public NetworkManager _manager;

    public DBGameManager _dbgm;

    public OptitrackHmd _optiHMD;

    public OptitrackRigidBody _OptiRigid;

    [SerializeField]
    public bool configuration = false;
    [SerializeField]
    public int offsetX;
    [SerializeField]
    public int offsetY;
    [SerializeField]
    string optiServer;
    [SerializeField]
    string optiClient;
    [SerializeField]
    string serverAddress;
    [SerializeField]
    public int hmdnumber = 0;
    [SerializeField]
    public string[] hmdStrings = new string[] { "hmd1", "hmd2", "hmd3", "hmd4" };
    
    [SerializeField]
    string OptiError = "";

    void Awake()
    {
        if(PlayerPrefs.GetString("OptiServer") != null)
            optiServer = PlayerPrefs.GetString("OptiServer");
        if (PlayerPrefs.GetString("OptiClient") != null)
            optiClient = PlayerPrefs.GetString("OptiClient");
        if (PlayerPrefs.GetString("GameServerAddress") != null)
            serverAddress = PlayerPrefs.GetString("GameServerAddress");
        
        if(_manager==null)
            _manager = GetComponent<NetworkManager>();
        if (_osc == null)
            _osc = OptitrackStreamingClient.FindDefaultClient();
        _osc.ServerAddress = optiServer;
        _osc.LocalAddress = optiClient;
        _manager.networkAddress = serverAddress;
    }

    // Use this for initialization
    void Start () {
		
	}
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.H))
        {
            hidden = !hidden;
        }
	}

    private void OnGUI()
    {
        if (hidden) return;
        string configText = "Configure";
        int xpos = 10 + offsetX;
        int ypos = 40 + offsetY;
        int spacing = 24;
        configText = "Configure";
        if (!NetworkClient.active && !NetworkServer.active && _manager.matchMaker == null)
        {
            if (configuration)
            {
                configText = "Back";
                if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Optitrack Streaming Server"))
                {
                    PlayerPrefs.SetString("OptiServer", optiServer);
                    _osc.ServerAddress = optiServer;
                }
                optiServer = GUI.TextField(new Rect(xpos + 205, ypos, 95, 20), optiServer);
                ypos += spacing;
                if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Optitrack Streaming Client"))
                {
                    PlayerPrefs.SetString("OptiClient", optiClient);
                    _osc.LocalAddress = optiClient;
                }
                optiClient = GUI.TextField(new Rect(xpos + 205, ypos, 95, 20), optiClient);
                ypos += spacing;
            }
            else
            {
                {
                    if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Host Game(H)"))
                    {
                        _manager.StartHost();
                    }
                    hmdnumber = GUI.SelectionGrid(new Rect(xpos + 210, ypos, 100, 100), hmdnumber, hmdStrings, 2);
                    ypos += spacing;


                    if (GUI.Button(new Rect(xpos, ypos, 105, 20), "Join Game(C)"))
                    {
                        JoinGame(true);
                    }
                    _manager.networkAddress = serverAddress;
                    serverAddress = GUI.TextField(new Rect(xpos + 100, ypos, 95, 20), serverAddress);
                    ypos += spacing;
                    if (GUI.Button(new Rect(xpos, ypos, 200, 20), "LAN Server Only(S)"))
                    {
                        _manager.StartServer();
                    }
                    ypos += spacing;
                }
            }
        }
        else
        {
            if (NetworkServer.active)
            {
                GUI.Label(new Rect(xpos, ypos, 300, 20), "Server: port=" + _manager.networkPort);
                ypos += spacing;
            }
            if (NetworkClient.active)
            {
                GUI.Label(new Rect(xpos, ypos, 300, 20), "Client: address=" + _manager.networkAddress + " port=" + _manager.networkPort);
                ypos += spacing;
            }
        }
        if (GUI.Button(new Rect(xpos, ypos, 200, 20), configText))
        {
            configuration = !configuration;
        }
        ypos += spacing;
        if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Start/Stop Optitrack Stream"))
        {
            if (!_osc.enabled)
            {
                EnableOptitrackStreamingClient();
            }
            else
            {
                _osc.enabled = false;
            }
        }
        ypos += spacing;
        ypos += spacing;
        ypos += spacing;
        ypos += spacing;
        ypos += spacing;

        GUIStyle style = new GUIStyle();
        style.normal.textColor = _osc.enabled ? new Color(0.0f, 1.0f, 0.0f, 1.0f) : new Color(1.0f, 0.0f, 0.0f, 1.0f);
        GUI.Label(new Rect(xpos, ypos, 300, 20), OptiError, style);
        ypos += spacing;
        GUI.Label(new Rect(xpos, ypos, 300, 20), "Optitrack Status: " + (_osc.enabled ? "Streaming" : "Disabled"), style);
        ypos += spacing;
        GUI.Label(new Rect(xpos, ypos, 300, 20), "Optitrack Server IP: " + optiServer, style);
        ypos += spacing;
        GUI.Label(new Rect(xpos, ypos, 300, 20), "Optitrack Client IP:" + optiClient, style);
        ypos += spacing;

        if (NetworkServer.active || NetworkClient.active)
        {
            if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Stop (X)"))
            {
                HostGame(false);
            }
            ypos += spacing;
        }
    }

    void EnableOptitrackStreamingClient()
    {
        try
        {
            if(!_osc.enabled)
                _osc.enabled = true;
        }
        catch (NaturalPoint.NatNetLib.NatNetException ex)
        {
            Debug.Log(ex.ToString());
            OptiError = ex.ToString();
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OptiError = ex.ToString();
        }
    }

	void HostGame(bool withMocap)
	{
		_manager.StartHost();
	}

	void JoinGame(bool withMocap)
	{
		PlayerPrefs.SetString("GameServerAddress", serverAddress);
		((DBNetworkManager)DBNetworkManager.singleton)._clientMotiveRefID = (hmdnumber) + 1;
		_optiHMD.RigidBodyId = ((hmdnumber + 1) * 10) + 1;
		_OptiRigid = GetComponentInChildren<OptitrackRigidBody>();
		if (_OptiRigid != null)
		{
			_OptiRigid.RigidBodyId = ((hmdnumber + 1) * 10) + 1;
		}
		_manager.StartClient();
		if (NetworkClient.active)
		{

		}
	}
}

