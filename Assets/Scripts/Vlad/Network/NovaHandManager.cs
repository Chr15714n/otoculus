﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;

namespace Vlad {
    public class NovaHandManager : NetworkBehaviour {
        /*Variable Declaration*/
        [SyncVar]
        public int UniqueID=0;

        public List<Transform> _novaBalls;//found at start of game

        public Transform[] HandObjects;//physical hands of player

        [SyncVar(hook = "onBeingControlled")]
        public bool _ballControlled = false;
        [SyncVar]
        public bool _holdingButton = false;

        public Vector3 throwVelocity {
            get {
                if (VRDevice.isPresent)
                        return OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
                    else
                        return Vector3.Normalize(HandObjects[0].transform.forward + HandObjects[0].transform.up) * 2.5f;
            }
        }

        public GameObject PlayerBall;
        public GameObject PlayerBallAndJelly;
        //-----------//

        

        //----------//
       
        /*prdefined unity methods*/

        // Use this for initialization
        void Start() {
            _novaBalls = new List<Transform>();
            foreach(NovaBall n in FindObjectsOfType<NovaBall>()) {
                _novaBalls.Add(n.transform);
            }
        }

        // Update is called once per frame
        void Update() {
            if (!isLocalPlayer) return;
            if (VRDevice.isPresent) {
                if (OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.Three)) {
                    CmdHold();
                }
                if (_holdingButton &&
                    (OVRInput.GetUp(OVRInput.Button.One) || OVRInput.GetUp(OVRInput.Button.Three))
                ) {
                    CmdRelease();
                }
            }
            else {
                if (Input.GetMouseButtonDown(0)) {
                    CmdHold();
                }
                if (_holdingButton && Input.GetMouseButtonUp(0)) {
                    CmdRelease();
                }
            }
        }

        public override void OnStartLocalPlayer() {
            base.OnStartLocalPlayer();
        }
        /*custom methods*/

        public void onBeingControlled(bool controlled) {
            _ballControlled = controlled;
            if(controlled && isLocalPlayer)CmdServerSpawnPlayerBall();
        }

        [Command]
        public void CmdHold() {
            Debug.Log("CmdHold Player " + UniqueID);
            _holdingButton = true;
        }

        [Command]
        public void CmdRelease() {
            Debug.Log("CmdRelease Player " + UniqueID);
            _holdingButton = false;
            _ballControlled = false;
            RpcClientsDestroyBall();
        }

        [Command]
        public void CmdServerSpawnPlayerBall() {
            RpcClientsSpawnBall();
        }
        [ClientRpc]
        public void RpcClientsSpawnBall() {

            if (PlayerBall != null && PlayerBallAndJelly == null) {

                PlayerBallAndJelly = (GameObject)Instantiate(PlayerBall);
                PlayerBallAndJelly.transform.position = HandObjects[0].position;
                PlayerBallAndJelly.transform.SetParent(HandObjects[0]);
            }
        }

        [ClientRpc]
        public void RpcClientsDestroyBall() {
            if (PlayerBallAndJelly != null) {
                Destroy(PlayerBallAndJelly);
            }
        }
    }
}
/*
 
    

     
     */