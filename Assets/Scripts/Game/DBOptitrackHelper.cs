﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// UI Which allows a client to type in IPs which connect to the Optitrack Streaming service, and allows the user to select which HMD to use
/// </summary>
public class DBOptitrackHelper : MonoBehaviour {


    public OptitrackStreamingClient _osc;
    public DBGameManager _dbgm;
    public OptitrackHmd _optiHMD;
    public string _optitrackHostIP;
    public string _optitrackClientIP;

    public string _hmdID;
    private int selGridInt = 0;
    public string[] hmdStrings = new string[] { "hmd1", "hmd2", "hmd3", "hmd4" };
    
    // Use this for initialization
    void Start () {
        _optitrackHostIP = _osc.ServerAddress;
        _optitrackClientIP = _osc.LocalAddress;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGUI()
    {
        if(_dbgm.getState() == DBGameManager.DBGameState.NONE) { 
            GUI.Label(new Rect(25, 10, 110, 20),"Optitack Host IP");
            GUI.Label(new Rect(25, 40, 110, 20), "Client IP");
            _optitrackHostIP = GUI.TextField(new Rect(140, 10, 200, 20), _optitrackHostIP, 15); ;
            _optitrackClientIP = GUI.TextField(new Rect(140, 40, 200, 20), _optitrackClientIP, 15);
            if (GUI.Button(new Rect(350, 10, 50, 50), "Apply")){
                _osc.LocalAddress = _optitrackClientIP;
                _osc.ServerAddress = _optitrackHostIP;

                //((DBNetworkManager)DBNetworkManager.singleton)._clientHMDID = hmdStrings[selGridInt];

                //_playerNumber = ((selGridInt +1) * 10);
                _optiHMD.RigidBodyId = ((selGridInt + 1) * 10) + 1;
                _osc.enabled = true;
            }
            selGridInt = GUI.SelectionGrid(new Rect(350, 90, 150, 150), selGridInt, hmdStrings, 2);
        }
    }
}
