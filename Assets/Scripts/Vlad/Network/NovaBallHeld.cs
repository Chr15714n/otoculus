﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NovaBallHeld : NetworkBehaviour {

    public Transform _handReference;
    public int playerid;
    public GameObject activeBall;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_handReference == null) {
            Destroy(this);
            return;
        }
        this.transform.position = _handReference.position;
        this.transform.rotation = _handReference.rotation;
	}


    public void RpcReleaseBall() {
        activeBall = (GameObject)GameObject.Instantiate(activeBall, this.transform.position, Quaternion.identity);
        NetworkServer.Spawn(activeBall);
        Vlad.NovaHandsV2 nv = _handReference.GetComponentInParent<Vlad.NovaHandsV2>();

        if (nv._ballControlledLeft) {
            activeBall.GetComponent<Rigidbody>().velocity = _handReference.GetComponentInParent<Vlad.NovaHandsV2>().LeftVelocity;
        }
        else if (nv._ballControlledRight) {
            activeBall.GetComponent<Rigidbody>().velocity = _handReference.GetComponentInParent<Vlad.NovaHandsV2>().RightVelocity;
        }
        else {
            //idk?
        }
        nv._ballControlledRight = nv._ballControlledLeft = false;
        Destroy(this.gameObject);
    }

}
