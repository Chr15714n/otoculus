﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Morgan.Networking;
using Vlad.Networking;

namespace Morgan
{
    namespace Networking
    {

        public class NetworkBallManager : NetworkBehaviour
        {
            public bool held;

            [SyncVar(hook ="onHandHoldingChanged")]
            public Transform handHoldingMe;

            public Vector3 myHandVelocity = Vector3.zero;

            [SyncVar]
            public bool thrown = true;


            // Use this for initialization
            void Start()
            {

            }
            
            // Update is called once per frame
            void Update()
            {
                if (!held && !thrown)
                {
                    thrown = true;
                }
                
                if (held)
                {
                    CmdBallHeld();
                }
            }

            public void onHandHoldingChanged(Transform tra) {
                handHoldingMe = tra;
                if(handHoldingMe != null) { 
                    transform.localPosition = Vector3.zero;
                    GetComponent<Rigidbody>().position = transform.position;
                    GetComponent<Rigidbody>().Sleep();
                    thrown = false;
                }
            }

            public void OnTriggerEnter(Collider col)
            {               
                if (col.GetComponent<NetworkHandManager>())
                handHoldingMe = col.transform;
                
            }

            public void OnTriggerExit(Collider col)
            {
                if (col.GetComponent<NetworkHandManager>())
                handHoldingMe = null;
            }


            //[ClientRpc]
            //public void RpcBallGrabbed()
            //{
            //    held = true;
            //}


            //[ClientRpc]
            //void RpcBallHeld()
            //{
            //    transform.parent = handHoldingMe;

            //    //handHoldingMe.parent.GetComponents<NetworkTransformChild>()[4].target = transform;

            //    transform.localPosition = Vector3.zero;
            //    GetComponent<Rigidbody>().position = transform.position;
            //    GetComponent<Rigidbody>().Sleep();
            //    thrown = false;
            //}



            [Command]
            void CmdBallHeld() {
                transform.parent = handHoldingMe;

                //handHoldingMe.parent.GetComponents<NetworkTransformChild>()[4].target = transform;

                transform.localPosition = Vector3.zero;
                GetComponent<Rigidbody>().position = transform.position;
                GetComponent<Rigidbody>().Sleep();


                myHandVelocity = handHoldingMe.GetComponent<NetworkHandManager>().handVelocity;

                thrown = false;
            }

            //[ClientRpc]
            //public void RpcParentClear()
            //{
            //     transform.parent = null;

            //}

            //[ClientRpc]
            //public void RpcBallThrown()
            //{
            //    held = false;
            //}

            //[ClientRpc]
            //public void RpcBallLaunch()
            //{
            //    GetComponent<Rigidbody>().velocity = myHandVelocity;
            //}
        }
    }
}

