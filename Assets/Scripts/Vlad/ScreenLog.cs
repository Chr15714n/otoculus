﻿using UnityEngine;
using System.Collections;

public class ScreenLog : MonoBehaviour {
    string myLog;
    Queue myLogQueue = new Queue();
    public int logsize=50;
    private uint lognum = 0;
    public Vector2 scrollPosition;
    public Rect windowRect;
    public bool _toggleLog = false;
    void Start() {
        windowRect = new Rect(Screen.width / 2, 10 / 2, 230, 360);
    }

    public void _ShowHideOnScreenLog() {
        _toggleLog = !_toggleLog;
    }

    public void Update() {
        if (Input.GetKeyDown(KeyCode.L)) {
            _ShowHideOnScreenLog();
        }
        if(myLogQueue.Count > logsize) {
            myLogQueue.Dequeue();
        }
    }

    void OnEnable() {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable() {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type) {
        myLog = logString;
        string newString = "\n "+ ++lognum+": [" + type + "] : " + myLog;
        myLogQueue.Enqueue(newString);
        if (type == LogType.Exception) {
            newString = "\n" + stackTrace;
            myLogQueue.Enqueue(newString);
        }
        myLog = string.Empty;
        foreach (string mylog in myLogQueue) {
            myLog += mylog;
        }
    }

    void OnGUI() {
        if (_toggleLog) {
            windowRect = GUI.Window(0, windowRect, DoMyWindow, "Debug.Log");
            


        }

    }

    void DoMyWindow(int windowID) {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(200), GUILayout.Height(300));
        GUILayout.Label(myLog);
        GUILayout.EndScrollView();
        GUI.DragWindow(new Rect(0, 0, 10000, 10000));
    }
}
