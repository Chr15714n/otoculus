﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Vlad.Networking;
using UnityEngine.VR;

namespace Morgan
{
    namespace Networking
    {
        public class NetworkHandManager : NetworkBehaviour
        {

            public Transform networkBall;

           

            private bool _holdingBall;
            public bool holdingBall
            {
                get
                {
                    return _holdingBall;
                }
                set
                {
                    _holdingBall = value;
                }
            }


            public bool holdButton {
                get {
                    if (VRDevice.isPresent)
                        return OVRInput.Get(OVRInput.Button.One);
                    else
                        return Input.GetMouseButton(0);
                }

            }


            public bool DEBUGBOOL;
            [Range(1,20)]
            public float DebugVelocity;
            public Vector3 handVelocity {

                get {
                    if (VRDevice.isPresent)
                        return OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
                    else
                        return Vector3.Normalize(transform.forward + transform.up) * DebugVelocity;
                }

            }

            void Start ()
            {
                
                if (!GetComponentInParent<NetworkIdentity>().isLocalPlayer)
                {
                    this.GetComponent<CapsuleCollider>().enabled = false;
                    //this is the essential of it. set enabled to false will prevent the Update function from being called.
                    this.enabled = false;
                }
                networkBall = GameObject.Find("NetworkBall").transform;
            }
            // Update is called once per frame
            void Update()
            {
                /*DEBUG*/
                DEBUGBOOL = holdButton;


                if (!holdingBall)
                {
                    if (networkBall.GetComponent<NetworkBallManager>().handHoldingMe != null && networkBall.GetComponent<NetworkBallManager>().handHoldingMe == this.transform)
                    {
                        if (holdButton)
                        {
                            if (holdingBall != true)
                            {
                                holdingBall = true;
                            }

                            //TELLS THE BALL ITS HELD
                            networkBall.GetComponent<NetworkBallManager>().held = true;
                            
                        }
                    }

                }

                if (holdingBall)
                {
                    if (!holdButton)
                    {
                        holdingBall = false;
                        networkBall.GetComponent<NetworkBallManager>().held = false;
                        networkBall.parent = null;
                    }
                }
            }

           

            
            void OnTriggerEnter(Collider col)
            {
         
            }


            void OnTriggerExit(Collider col)
            {

            }

           
            //void NetworkGrab()
            //{
            //    Debug.Log("GRABBBB you bitch");
            //    networkBall.GetComponent<NetworkBallManager>().RpcBallGrabbed();
            //}

          
            //void NetworkRelease()
            //{
            //    networkBall.GetComponent<NetworkBallManager>().RpcBallThrown();
            //}

           
            //public void BallHolderUpdate(GameObject trans)
            //{

            //    networkBall.GetComponent<NetworkBallManager>().handHoldingMe = trans.transform;

            //}
        }
    }
}