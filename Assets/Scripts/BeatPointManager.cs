﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BeatPointManager : MonoBehaviour {

    [BoxGroup("Player Info")]
    public float maxDistance;

    [BoxGroup("States")]
    public bool beatActive = false;

    [BoxGroup("Components")]
    public GameManager gameManager;
    //public BeatGuide[] beatGuides;
    [BoxGroup("Components")]
    public Conductor conductor;
    [BoxGroup("Components")]
    public Animator anim;
    [BoxGroup("Components")]
    public Floor myTile;
    [BoxGroup("States")]
    public bool powerUp;
    [BoxGroup("Components")]
    public SphereCollider myCollider;
    [BoxGroup("Player Info")]
    public PlayerManager myPlayer;

	// Use this for initialization
	void Start () {
        
        myTile = GetComponentInParent<Floor>();
        PlayerGet();
        myCollider = gameObject.GetComponent<SphereCollider>();
        gameManager = FindObjectOfType<GameManager>();
        conductor = GameObject.Find("Conductor").GetComponent<Conductor>();
        //beatGuides = GetComponentsInChildren<BeatGuide>();
        anim = gameObject.GetComponent<Animator>();

        myCollider.isTrigger = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (myPlayer == null) myPlayer = myTile.myPlayer;
        if (powerUp) {
            return;
        }
        

        if (conductor.currentBeats == 1)
        {
            myCollider.isTrigger = false;
            anim.SetTrigger("_beat2");
            anim.ResetTrigger("_beat1");
            anim.ResetTrigger("_beat3");
            anim.ResetTrigger("_beat4");
        }

        if (conductor.currentBeats == 3)
        {
            anim.SetTrigger("_beat3");
            anim.ResetTrigger("_beat1");
            anim.ResetTrigger("_beat2");
            anim.ResetTrigger("_beat4");
        }

        if (conductor.currentBeats == 5)
        {
            anim.SetTrigger("_beat4");
            anim.ResetTrigger("_beat2");
            anim.ResetTrigger("_beat1");
            anim.ResetTrigger("_beat3");
        }

        if (conductor.currentBeats >= 7)
        {
            myCollider.isTrigger = true;
            anim.SetTrigger("_beat1");
            anim.ResetTrigger("_beat2");
            anim.ResetTrigger("_beat3");
            anim.ResetTrigger("_beat4");
            beatActive = true;
            gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
        }
        else
        {
            beatActive = false;
            gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Hand>() && beatActive)
        {
            powerUp = true;
            myTile.spawnTime = true;
            myCollider.isTrigger = false;
            //Destroy(gameObject);
        }
    }

    public void PlayerGet()
    {
        List < PlayerManager > playerList = new List<PlayerManager>(GameObject.FindObjectsOfType<PlayerManager>());
        List<float> playerDistances = new List<float>();
        int closestIndex = 0;
        float closestDistance = 0;
        if (playerList.ToArray().Length < 1) return;

        foreach (PlayerManager player in playerList)
        {
            float distance = Vector3.Distance(myTile.gameObject.transform.position, player.transform.position);
            playerDistances.Add(distance);
        }

        //Debug.Log(playerList.ToArray().Length);

        if (playerDistances.ToArray().Length > 0)
        {
            closestDistance = Mathf.Min(playerDistances.ToArray());

            int i = 0;

            foreach (float distancesz in playerDistances)
            {
                if (closestDistance == playerDistances[i])
                {
                    closestIndex = i;
                }
                i++;
            }


        }
        //Debug.Log(closestDistance);
        if (closestDistance <= maxDistance)
        myPlayer = playerList[closestIndex].GetComponent<PlayerManager>();
        else myPlayer = null;

    }

}
