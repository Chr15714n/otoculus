﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


[System.Serializable]
public class DBMotiveEntry
{
    public int _motiveReferenceID;
    public int _motiveRigidbodyID;
    public string _motiveSkeletonName;
}

public class DBNetworkManager : NetworkManager
{

	public Camera _mainCamera;
	public GameObject _hostPlayerPrefab;
	public GameObject _hostPlayerInstance;
	public GameObject _clientPlayerPrefab;
	public DBGameManager _gameManager;
    public OptitrackStreamingClient _optitrackStream;
    public List<DBMotiveEntry> _motiveEntries = new List<DBMotiveEntry>();
    public int _clientMotiveRefID;

	public override void OnServerConnect(NetworkConnection conn)
	{
		base.OnServerConnect(conn);

		Debug.Log("OnServerConnect: " + conn.ToString());

        if(_optitrackStream == null)
        {
            _optitrackStream = OptitrackStreamingClient.FindDefaultClient();

        }
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{

		Debug.Log("OnServerAddPlayer: " + conn.ToString());
		Debug.Log("PlayerControllerID: " + playerControllerId);

		// Disable main camera.
		if (_mainCamera != null)
		{
			_mainCamera.gameObject.SetActive(false);
		}

		if (conn.hostId == -1)
		{
			// The host player. Don't need to create anything as the flying camera should be enabled automatically.
			_hostPlayerInstance = (GameObject)Instantiate(_hostPlayerPrefab, Vector3.zero, Quaternion.identity);
			NetworkServer.AddPlayerForConnection(conn, _hostPlayerInstance, playerControllerId);
		}
		else
		{
			// Actual client players. Use the player prefab to create a controller.
			GameObject newPlayer = (GameObject)Instantiate(_clientPlayerPrefab, Vector3.zero, Quaternion.identity);
            DBPlayerNetworkComponent dbPlayer = newPlayer.GetComponent<DBPlayerNetworkComponent>();
			NetworkServer.AddPlayerForConnection(conn, newPlayer, playerControllerId);
			dbPlayer.ConnectionID = conn.connectionId;
			_gameManager.AddPlayer(dbPlayer);
		}
	}

	public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
	{
		Debug.Log("OnServerRemovePlayer with conn.hostId: " + conn.hostId);
		_gameManager.RemovePlayer(conn.connectionId);
		base.OnServerRemovePlayer(conn, player);
	}

    public override void OnStartServer()
	{
		base.OnStartServer();
		Debug.Log("OnStartServer");
	}

	public override void OnServerDisconnect(NetworkConnection conn)
	{
		base.OnServerDisconnect(conn);
        _gameManager.ResetGameManager();
		Debug.Log("OnServerDisconnect");
	}

	public override void OnStopServer()
	{
		base.OnStopServer();
        _gameManager.ResetGameManager();
        Debug.Log("OnStopServer");
		// Enable main camera.
		if (_mainCamera != null)
		{
			_mainCamera.gameObject.SetActive(true);
		}
	}

	public override void OnClientConnect(NetworkConnection conn)
	{

		base.OnClientConnect(conn);

		Debug.Log("OnClientConnect: " + conn);

		// Disable main camera
		//_mainCamera.gameObject.SetActive(false);
	}

	public override void OnClientDisconnect(NetworkConnection conn)
	{
		base.OnClientDisconnect(conn);

		Debug.Log("OnClientDisconnect: " + conn);

		// Disable main camera
		_mainCamera.gameObject.SetActive(true);
	}

	public override void OnClientError(NetworkConnection conn, int errorCode)
	{
		base.OnClientError(conn, errorCode);
		Debug.Log("OnClientError: " + conn + ", error code: " + errorCode);
	}

    public int GetMotiveID(int MotiveReferenceID)
    {
		int index = MotiveReferenceID - 1;
		DBNetworkManager networkManager = ((DBNetworkManager)DBNetworkManager.singleton);
		if(index >= 0 && index < networkManager._motiveEntries.Count)
		{
			return networkManager._motiveEntries[index]._motiveReferenceID;
		}
		return 0;
    }
}