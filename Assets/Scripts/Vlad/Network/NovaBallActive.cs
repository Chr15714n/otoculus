﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class NovaBallActive : NetworkBehaviour {

    public Vlad.Networking.NetworkGameManager _netManager;

    public Rigidbody _ballRigidBody;
    public GameObject heldBall;

    public List<Transform> _handsOnBall = new List<Transform>();
    public Transform currentHand;

    [SyncVar]
    public int _heldPlayerID = -1;

    // Use this for initialization
    void Start() {
        _heldPlayerID = -1;
        //if the ball doesn't have a rigid body, then destroy it, because it's the wrong version of the ball
        _ballRigidBody = GetComponent<Rigidbody>();
        if (_ballRigidBody == null) {
            Destroy(this);
            return;
        }
        if (_netManager == null) {
            _netManager = FindObjectOfType<Vlad.Networking.NetworkGameManager>();
        }
    }

    // Update is called once per frame
    void Update() {

        if(isServer && _heldPlayerID >= 0) {
            Debug.Log("Grabbed");
            heldBall = (GameObject)GameObject.Instantiate(heldBall, Vector3.zero, Quaternion.identity);
            NetworkServer.Spawn(heldBall);
            NovaBallHeld nvh = heldBall.GetComponent<NovaBallHeld>();
            Vlad.NovaHandsV2 nvhand = currentHand.GetComponentInParent<Vlad.NovaHandsV2>();
            nvh.playerid = _heldPlayerID;
            nvh._handReference = currentHand;
            nvhand.heldBall = nvh;
            Destroy(this.gameObject);
        }

        if (_handsOnBall.Count > 0 && _heldPlayerID < 0) {
            foreach (Transform hand in _handsOnBall) {
                Vlad.NovaHandsV2 novahand = hand.GetComponentInParent<Vlad.NovaHandsV2>();
                if (novahand._holdingButtonLeft || novahand._holdingButtonRight) {
                    print("Button Down");
                    if (novahand._leftHand == hand) {
                        currentHand = novahand._leftHand;
                        novahand._ballControlledLeft = true;
                    }
                    else if (novahand._rightHand == hand) {
                        currentHand = novahand._rightHand;
                        novahand._ballControlledRight = true;
                    }
                    else {
                        currentHand = null;
                        _heldPlayerID = -1;
                        continue;
                    }
                    _heldPlayerID = novahand._uniqueID;
                    
                }

            }//loop
        }//if
    }//void

    public void OnTriggerEnter(Collider col) {
        if (col.tag.Equals("PlayerHand") && !_handsOnBall.Contains(col.transform)) {
            _handsOnBall.Add(col.transform);
        }
    }


    public void OnTriggerExit(Collider col) {
        if (col.tag.Equals("PlayerHand")) {
            _handsOnBall.Remove(col.transform);
        }
    }



    [ClientRpc]
    public void RpcParentBall() {

        Debug.Log("Grabbed");
        heldBall = (GameObject)GameObject.Instantiate(heldBall, Vector3.zero, Quaternion.identity);
        NetworkServer.Spawn(heldBall);
        NovaBallHeld nvh = heldBall.GetComponent<NovaBallHeld>();
        Vlad.NovaHandsV2 nvhand = currentHand.GetComponentInParent<Vlad.NovaHandsV2>();
        nvh.playerid = _heldPlayerID;
        nvh._handReference = currentHand;
        nvhand.heldBall = nvh;
        Destroy(this.gameObject);

    }

    [Command]
    public void CmdParentBall() {

        Debug.Log("Grabbed");
        heldBall = (GameObject)GameObject.Instantiate(heldBall, Vector3.zero, Quaternion.identity);
        NetworkServer.Spawn(heldBall);
        NovaBallHeld nvh = heldBall.GetComponent<NovaBallHeld>();
        Vlad.NovaHandsV2 nvhand = currentHand.GetComponentInParent<Vlad.NovaHandsV2>();
        nvh.playerid = _heldPlayerID;
        nvh._handReference = currentHand;
        nvhand.heldBall = nvh;
        Destroy(this.gameObject);
    }



}
