﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GameStateInformation : MonoBehaviour {

    Conductor conductor;
    [Header("Song Information")]
    public float BPM;
    public string songName = "Song Name Here";
    public string composer = "Composter Name Here";

    [Header("Current Round Information")]
    public string currentRound;
    public float currentRoundLength;
    public string nextRound;

    [Header("All Round Information")]
    // public string[] gameRounds = new string[4];
    public float totalGameTime;
    public float[] roundLengthsInSeconds;
    

    public bool updateMe;

    // Use this for initialization
    void Start () {
        
        conductor = GameObject.Find("Conductor").GetComponent<Conductor>();
        updateMe = true;
        roundLengthsInSeconds = new float[conductor.roundNames.Length];
    }
	
	// Update is called once per frame
	void Update () {
        currentRound = conductor.roundNames[conductor.currentRound];
        nextRound = conductor.roundNames[conductor.currentRound + 1];

        if (updateMe)
        {
            totalGameTime = 0;
            BPM = conductor.BPM;     
            RoundLengthUpdater();
            
    updateMe = false;
        }

        if (Application.isPlaying)
        {
            currentRoundLength -= Time.deltaTime;

            if (currentRoundLength <= 0)
            {
                //updateMe = true;
                currentRoundLength = roundLengthsInSeconds[conductor.currentRound+1];
            }
        }


    }

    void RoundLengthUpdater()
    {
        roundLengthsInSeconds = new float[conductor.roundNames.Length];
        for (int i = 0; i<conductor.roundNames.Length; i++)
        {
            float timePerBeat = 60/BPM;
            roundLengthsInSeconds[i] = conductor.roundLengths[i] * 2 * timePerBeat;
            totalGameTime += roundLengthsInSeconds[i];
            currentRoundLength = roundLengthsInSeconds[conductor.currentRound];
        }
       
        


    }



}
