﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBBall : NetworkBehaviour
{
	[SyncVar(hook = "OnTeamChange")]
	public DBTeamInfo.TeamID _teamID;

    [SyncVar(hook = "OnMotiveIDChange")]
    public int _motiveID;

	

	public enum DBBallState
	{
		CHARGE00 = 0,			// Not currently in game
		CHARGE01 = 1,		    // In game, this is the Ball's default active state, worth 1
		CHARGE02 = 2,			// The Ball has received it's first power charge, worth 2
        CHARGE03 = 3,           // The Ball has received it's second power charge, worth 3
        CHARGE04 = 4,           // The Ball has received it's third power charge, worth 4
		CHARGE05 = 5,			// Max Charge level for the ball, worth 5
	}    
    
    [SyncVar(hook = "OnStateChange")]
	public DBBallState _ballState;
    int _stateCast;
    int _stateBlock;
    int _powerCast;

    public DBTriggerComponent _triggerComponent;

    void Start()
	{
        _stateCast = (int)_ballState + _powerCast;
        _stateBlock = (int)_ballState;
        //find optitrack streaming client
        if (DBNetworkManager.singleton != null)
        {
            OptitrackRigidBody orb = GetComponent<OptitrackRigidBody>();
            if (orb != null)
            {
                orb.StreamingClient = OptitrackStreamingClient.FindDefaultClient();
            }
        }
        DBGameManager.Singleton.AddGameBall(this);
	}

    private void Update()
    {
        if(_stateBlock >= 5)
        {
            _ballState = (DBBallState)5;
        }

        if(_stateBlock <= 0)
        {
            _ballState = (DBBallState)1;
        }
    }

    void OnDestroy()
	{
		if(DBGameManager.Singleton != null)
		{
			DBGameManager.Singleton.RemoveGameBall(this);
		}
	}

    /*
	public void SetBallCharged()
	{
		_ballState = DBBall.DBBallState.CHARGED;
	}
	*/
	private void OnTeamChange(DBTeamInfo.TeamID newTeam)
	{
		_teamID = newTeam;
		Color teamColor = DBTeamInfo.GetTeamColor(_teamID);
		GetComponentInChildren<MeshRenderer>().material.color = teamColor;
	}
	
	public void OnStateChange(DBBallState newState)
	{        
        _ballState = newState;
        		// Nullify trigger delegates if not part of game, or not on server
		if (_ballState == DBBallState.CHARGE00 || !isServer)
		{
			_triggerComponent._parentGameObject = null;
			_triggerComponent._notifyOnTriggerEnter = null;
			_triggerComponent._notifyOnTriggerStay = null;
			_triggerComponent._notifyOnTriggerExit = null;
		}
		else
		{
			// Set trigger delegates if in game and only on server
			_triggerComponent._parentGameObject = this.gameObject;
			_triggerComponent._notifyOnTriggerEnter = NotifyOnTriggerEnter;
			_triggerComponent._notifyOnTriggerStay = NotifyOnTriggerStay;
			_triggerComponent._notifyOnTriggerExit = NotifyOnTriggerExit;
		}

		if (newState == DBBallState.CHARGE01)
		{
			Color teamColor = DBTeamInfo.GetTeamColor(_teamID) * 0.1f;
			GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", teamColor);
		} else if(newState == DBBallState.CHARGE02)
        {
            Color teamColor = DBTeamInfo.GetTeamColor(_teamID) * 0.25f;
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", teamColor);
            //TODO: Trigger particle system
        }
        else if (newState == DBBallState.CHARGE03)
        {
            Color teamColor = DBTeamInfo.GetTeamColor(_teamID) * 0.50f;
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", teamColor);
            //TODO: Trigger particle system
        }
        else if (newState == DBBallState.CHARGE04)
        {
            Color teamColor = DBTeamInfo.GetTeamColor(_teamID) * 0.75f;
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", teamColor);
            //TODO: Trigger particle system
        }
        else if (newState == DBBallState.CHARGE05)
        {
            Color teamColor = DBTeamInfo.GetTeamColor(_teamID) * 1f;
            GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", teamColor);
            //TODO: Trigger particle system
        }
        else if(newState == DBBallState.CHARGE00)
		{
			GetComponentInChildren<MeshRenderer>().material.color = Color.black;
		}
		else
		{
			GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
		}
	}

	void NotifyOnTriggerEnter(DBTriggerComponent triggerComponent, Collider other)
	{
		if (isServer)
		{
			if (other.gameObject.CompareTag(DBGameManager.PlayerHandTag))
			{
				DBTriggerComponent otherTrigger = other.GetComponent<DBTriggerComponent>();
				if (otherTrigger != null)
				{
                    DBPlayerNetworkComponent player = otherTrigger._parentGameObject.GetComponent<DBPlayerNetworkComponent>();
					if (player != null)
					{
						if (player._teamID != _teamID)
						{
                            // Player hand triggered this. Start charging if not same team. And set to same team.
                            _powerCast = other.GetComponent<DBWarriorClass>()._myPowerLevel;
                            StartCoroutine(_shotClock(10)); //TODO: Add more than console print effect here
							_ballState = (DBBallState)_stateCast;
							_teamID = player._teamID;
						}
					}
				}
			}
			else if (other.gameObject.CompareTag(DBGameManager.GoalTag))
			{
				DBTriggerComponent otherTrigger = other.GetComponent<DBTriggerComponent>();
				if (otherTrigger != null)
				{
					DBGoal goal = otherTrigger._parentGameObject.GetComponent<DBGoal>();
					if (goal != null)
					{
						if(_ballState == DBBallState.CHARGE01 && _teamID != DBTeamInfo.TeamID.NONE && _teamID != goal._teamID)
						{
							// Score for team and decharge
							DBGameManager.Singleton.NotifyGoal(this, goal); //TODO: Update this to reflect NET damage
						}
					}
				}
			}
		}
	}

	void NotifyOnTriggerStay(DBTriggerComponent triggerComponent, Collider other)
	{
		if (isServer)
		{

		}
	}    

    void OnMotiveIDChange(int id)
    {
        _motiveID = id;
        OptitrackRigidBody optibody = GetComponent<OptitrackRigidBody>();
        if(optibody != null)
        {
            optibody.RigidBodyId = _motiveID;
        }
    }

	void NotifyOnTriggerExit(DBTriggerComponent triggerComponent, Collider other)
	{
		if (isServer)
		{
			// Player hand triggered this
			if (_ballState == DBBallState.CHARGE01 && other.gameObject.CompareTag(DBGameManager.PlayerHandTag))
			{
				DBTriggerComponent otherTrigger = other.GetComponent<DBTriggerComponent>();
				if (otherTrigger != null)
				{
                    DBPlayerNetworkComponent player = otherTrigger._parentGameObject.GetComponent<DBPlayerNetworkComponent>();
					if (player != null)
					{
                        return;
                        /*
						// Complete charge if same team
						if (player._teamID == _teamID && _ballState == DBBallState.CHARGE01)
						{
							player.CompleteBallCharge(this);
						}
                        */
					}
				}
			}
		}
	}

    private IEnumerator _shotClock(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Shot Clock!");
    }
}


