﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Morgan.Networking;
using UnityEngine.VR;


public class NovaNetworkPlayer : NetworkBehaviour {

    //position of model hands and head, set before instantiation
    public Transform TargetHead;
    public Transform TargetLeftHand;
    public Transform TargetRightHand;

    //Reference to local player script
    private PlayerManager LocalPlayer;

    //position of player's controllers and hmd, set at runtime
    private Transform _localHead { get; set; }
    private Transform _localLeft { get; set; }
    private Transform _localRight { get; set; }

	// Use this for initialization
	void Start () {
        //assign reference to local player's head and hands to private variables
        if (isLocalPlayer) {
            LocalPlayer = FindObjectOfType<PlayerManager>();
            //if VR device is present, use vr controls
            if (VRDevice.isPresent) {
                _localHead = GameObject.Find("CenterEyeAnchor").transform;
                _localLeft = GameObject.Find("LeftHandAnchor").transform;
                _localRight = GameObject.Find("RightHandAnchor").transform;
            }

            //if VR device isn't present, default to non-vr controls
            else {
                _localHead = GameObject.Find("NoHMD_DefaultCam").transform;
                _localLeft = GameObject.Find("DummyHand").transform;
                _localRight = GameObject.Find("DummyHand").transform;
            }
        }
    }//end of start
	
	// Update is called once per frame
	void Update () {
        if(isLocalPlayer && LocalPlayer != null) { 
            if (isLocalPlayer && _localHead != null) {
                TargetHead.transform.position = _localHead.position;
                TargetHead.transform.rotation = _localHead.rotation;
            }
            if (isLocalPlayer && _localLeft != null && TargetLeftHand != null) {
                TargetLeftHand.position = _localLeft.position;
                TargetLeftHand.rotation = _localLeft.rotation;
            }
            if (isLocalPlayer && _localRight != null && TargetRightHand != null) {
                TargetRightHand.position = _localRight.position;
                TargetRightHand.rotation = _localRight.rotation;
            }
        }
    }//end of update
}//end of class NovaNetworkPlayer
