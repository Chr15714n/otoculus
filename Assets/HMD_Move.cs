﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HMD_Move : MonoBehaviour {

    public float moveSpeed;
    public float rotSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        float horRot = Input.GetAxis("HorizontalRot");

        transform.position += new Vector3(hor*moveSpeed, 0, ver*moveSpeed);
        transform.eulerAngles += new Vector3(0, horRot * rotSpeed, 0);

	}
}
