﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBMusicConductor : NetworkBehaviour {

    private bool _musicPlaying;

    public bool _isPlaying { get { return _musicPlaying; } }

    private float _syncStartTime=0;

    public AudioClip[] _musicClips;
    public int _nextClipIndex;
    private AudioSource _musicSource;

    public int beatsPerBar;
    public bool tick;
    float timePerBar;
    float timePerBeat;
    public float clipTime;

    public int maxBeatsBeforeRespawn = 8;
    public int currentBeats = 0;
    public int maxBarsBeforeRespawn = 4;
    public int currentBar;
    public bool firstPlay = false;

    public TextMesh _testText;

    public bool powerUpPhase = true;

    // Use this for initialization
    void Start () {
        _musicSource = GetComponent<AudioSource>();
        timePerBar = clipTime;        
        NextClip();
    }
	
	// Update is called once per frame
	void Update () {
        if (!_isPlaying) return;

        if (clipTime <= 0)
        {
            
            clipTime = _musicClips[_nextClipIndex].length;

            timePerBar = clipTime;
            timePerBeat = timePerBar / beatsPerBar;
            //_testText.text = "TPB: " + timePerBeat;

            if (isServer)
                RpcPlayMusic(_nextClipIndex);


            NextClip();

            currentBar += 1;

            if (currentBar > 4)
            {
                currentBar = 1;
                powerUpPhase = !powerUpPhase;
            }
        }

        clipTime -= Time.deltaTime;
        timePerBeat -= Time.deltaTime;

        if (timePerBeat <= 0)
        {
            tick = !tick;
            currentBeats += 1;
            timePerBeat = timePerBar / beatsPerBar;
        }
    }
    


    private void LateUpdate()
    {
        if (firstPlay)
        {
            //currentBeats = 1;
            //firstPlay = false;
        }
    }



    //
    public void startMusic()
    {
        _musicPlaying = true;
        if(isServer)
        {
            _syncStartTime = Time.realtimeSinceStartup;
            //currentBeats = 0;
            RpcPlayMusic(_nextClipIndex);
        }
        else
        {
            _musicPlaying = false;
        }
        currentBeats = 1;
    }



    public void stopMusic()
    {
        _musicPlaying = false;
        if (isServer)
        {
            RpcStopMusic();
        }
        else
        {
            _musicPlaying = false;
        }
    }



    [ClientRpc]
    public void RpcPlayMusic(int clip)
    {
        if (_musicSource != null)
        {
            
            _musicSource.clip = _musicClips[clip];
            _musicSource.Play();
            //CmdClientMusicStarted("");

            tick = true;
            if (currentBeats >= maxBeatsBeforeRespawn)
            {
                currentBeats = 0;
            }
            currentBeats = 1;

            _musicPlaying = true;
        }
    }




    [ClientRpc]
    public void RpcStopMusic()
    {
        if (_musicSource != null)
        {
            _musicSource.Stop();
        }

        _musicPlaying = false;
    }



    [Command]
    public void CmdClientMusicStarted(string clientID)
    {
        float timeRecieved = Time.realtimeSinceStartup;
        float delta = timeRecieved - _syncStartTime;
        //Debug.Log(clientID + " Delta Time: " + delta);
    }




    public void NextClip()
    {
        _nextClipIndex =  Mathf.FloorToInt(Random.value * _musicClips.Length);
    }


    /*
    [ClientRpc]
    public void RpcBeatUpdate()
    {
        GlyphManager[] gms = GameObject.FindObjectsOfType<GlyphManager>();

        foreach(GlyphManager gm in gms)
        {
            gm.Beat = currentBeats;
        }

        GlyphGenerator[] ggs = GameObject.FindObjectsOfType<GlyphGenerator>();

        foreach(GlyphGenerator gg in ggs)
        {
            gg.Beat = currentBeats -1;
        }
    
        //Beat = _conductor.currentBeats - 1;
    }
    */


}
