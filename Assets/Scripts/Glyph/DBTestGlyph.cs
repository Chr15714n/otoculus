﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBTestGlyph : NetworkBehaviour
{
	public DBMusicConductor _musicConductor;

	//public DBGlyph _glyph;

	public DBGlyphGenerator _glyphGenerator;

	private bool _gameStarted;


	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.S))
		{
			if(hasAuthority)
			{
				RpcStartGame();
			}
		}
		else if(Input.GetKeyDown(KeyCode.G))
		{
			/*
			if (_glyph.GlyphActive)
			{
				_glyph.DeactivateGlyph();
			}
			else
			{
				_glyph.ActivateGlyph();
			}
			*/
		}
	}

	[ClientRpc]
	public void RpcStartGame()
	{
		if(_gameStarted)
		{
			return;
		}
		_gameStarted = true;

		_musicConductor = GameObject.Find("MusicConductor").GetComponent<DBMusicConductor>();
		if (_musicConductor != null)
		{
			_musicConductor.startMusic();
		}
	}

	[ClientRpc]
	public void RpcStopGame()
	{
		if (!_gameStarted)
		{
			return;
		}
		_gameStarted = false;

		if (_musicConductor != null)
		{
			_musicConductor.stopMusic();
		}
	}

	private void OnGUI()
	{
		if (isServer)
		{
			if (!_gameStarted)
			{
				if (GUI.Button(new Rect(180, 10, 100, 60), "Start\nGame"))
				{
					RpcStartGame();
				}
			}
			else
			{
				if (GUI.Button(new Rect(180, 10, 100, 60), "Stop\nGame"))
				{
					RpcStopGame();
				}
			}
		}
	}
}
