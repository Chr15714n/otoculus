﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Contains game logic responsible for controling anything required for core gameplay.
/// This includes Gamestates, Player Score, Player references, Ball references, goal references, and syncronizing values to clients.
/// </summary>
public class DBGameManager : NetworkBehaviour
{
    public bool _showingOptions = true;
    private Rect _windowRect;

    /// <summary>
    /// The Games different states while in an online game
    /// </summary>
	public enum DBGameState
	{
		NONE,
		LOBBY,
		IN_GAME,
		END_GAME
	}

    /// <summary>
    /// Reference to the object which displays the amount of time in seconds during the <b>IN_GAME</b> game state
    /// </summary>
    public TextMesh _timerText;

    /// <summary>
    /// The syncronized value of when the <b>Timer</b> should start
    /// </summary>
    [SyncVar] 
    public float _startTimeSeconds;

    /// <summary>
    /// The amount of time gone by after the <b>IN_GAME</b> game state has been invoked
    /// </summary>
    [SyncVar]
    private float _countdown;

    /// <summary>
    /// boolean controling when the <b>Timer</b> should start
    /// </summary>
    private bool _timerStarted = false;
	
    // Tags

    /// <summary>
    /// String containing the tag <b>Ball</b>
    /// </summary>
	public static string BallTag = "Ball";

    /// <summary>
    /// String containing the tag <b>Goal</b>
    /// </summary>
	public static string GoalTag = "Goal";

    /// <summary>
    /// String containing the tag <b>PlayerBody</b>
    /// </summary>
    public static string PlayerBodyTag = "PlayerBody";

    /// <summary>
    /// String containing the tag <b>PlayerHand</b>
    /// </summary>
	public static string PlayerHandTag = "PlayerHand";

    /// <summary>
    /// List containing reference to all objects of <b>DBTeamInfo</b>
    /// </summary>
	public List<DBTeamInfo> _teamInfos = new List<DBTeamInfo>();

    /// <summary>
    /// Syncronized value for the current gamestate
    /// </summary>
	[SyncVar(hook = "OnGameStateChange")]
	public DBGameState _gameState;

    /// <summary>
    /// List containing reference to all objects of <b>DBPlayer</b>
    /// </summary>
    /// 
    //[SyncVar(hook = "OnPlayerListChange")]
	private List<DBPlayerNetworkComponent> _players = new List<DBPlayerNetworkComponent>();

    /// <summary>
    /// List containing reference to all objects of <b>DBBall</b>
    /// </summary>
	private List<DBBall> _balls = new List<DBBall>();

    public DBMusicConductor _conductor;
    
    /// <summary>
    /// instance of the <b>DBGameManager</b> to ensure only one can exist at a time
    /// </summary>
	private static DBGameManager _singleton;

	public delegate void NotifyGameStateChange(DBGameState newState);
	public NotifyGameStateChange _notifyGameStateChange;

    ///
    

    /// <summary>
    /// Creates a new <b>DBGameManager</b> if one doesn't exist. References <b>DBGameManager</b>'s singlieton constructior named <b>_singleton</b>
    /// </summary>
	public static DBGameManager Singleton
	{
		get
		{
			return _singleton;
		}
	}

    [System.Obsolete("Code not in use", false)]
    public void OnPlayerListChange(List<DBPlayerNetworkComponent> newPlayerList)
    {
        _players = newPlayerList;
    }

	public void Awake()
	{
		if(_singleton != null && _singleton != this)
		{
			Debug.LogWarning("More than 1 DBGameManager exists!");
			return;
		}
		_singleton = this;
	}

    /// <summary>
    /// Default Unity Start Method
    /// </summary>
    public void Start()
    {
        if (isServer)
        {
            _windowRect = new Rect(Screen.width / 2, Screen.height / 2, 350, 100);
        }
    }

    /// <summary>
    /// Default Unity Update Method
    /// </summary>
    public void Update()
    {
        _timerText.text = string.Format("{0:0}", _countdown);
        if(_timerStarted) { 
            _countdown += (Time.deltaTime);
        }
    }

    /// <summary>
    /// Ensures the <b>DBGameManager</b> is null when the object is destroyed
    /// </summary>
    private void OnDestroy()
	{
		if(_singleton == this)
		{
			_singleton = null;
		}
	}

    /// <summary>
    /// Syncs the <b>GameStates</b> between server and clients
    /// </summary>
    /// <param name="newState">The New <b>GameState</b> which is passed on to the clients</param>
	public void OnGameStateChange(DBGameState newState)
	{
		Debug.Log("OnGameStateChange: " + newState);
		_gameState = newState;

		if(_notifyGameStateChange != null)
		{
			_notifyGameStateChange(newState);
		}
	}

    /// <summary>
    /// Debug Gui applied to server to chenge <b>GameState</b>s and start the timer
    /// Debug Gui applied to clients to display gamestates
    /// Debug Gui to display <b>DBPlayers</b> on the server
    /// Debug Gui to display which team the <b>DBPlayer</b> belongs to on their specific client
    /// </summary>
	private void OnGUI()
	{
		if (isServer)
		{

            if (_showingOptions)
            {
                _windowRect = GUI.Window(0, _windowRect, SetOptionsWindow, "Server Options");
            }
			// Show number of players and team
			string playerStr = "Players:\n";
			int numPlayers = _players.Count;
			for(int i = 0; i < numPlayers; ++i)
			{
				playerStr += string.Format("{0} : {1}\n", _players[i]._motiveReferenceID, _players[i]._teamID);
			}
			GUI.Label(new Rect(Screen.width - 100, 100, 100, 60), playerStr);
		}
		else
		{
			
		}

		GUI.Label(new Rect(Screen.width - 100, 10, 100, 60), "GameState\n" + _gameState);
	}

    void SetOptionsWindow(int windowID)
    {
        if (isServer) { 
            if (_gameState == DBGameState.IN_GAME)
            {
                if (GUI.Button(new Rect(10, 30, 100, 60), "Stop Game"))
                {
                    StopGamePlay();
                    _timerStarted = false;
                    _countdown = _startTimeSeconds;
                }
            }
            else
            {
                if (GUI.Button(new Rect(10, 30, 100, 60), "Start Game"))
                {
                    //AssignTeams();
                    StartGamePlay();
                    _timerStarted = true;
                }

                if (GUI.Button(new Rect(120, 30, 100, 60), "Assign Teams"))
                {
                    GetPlayerzoneGoal();
                    //AssignTeams();
                }
                if (GUI.Button(new Rect(230, 30, 100, 60), "Update Skeleton\nDefinition"))
                {
                    RpcUpdateSkeletonDefinition();
                }
            }
        }
        GUI.DragWindow(new Rect(0, 0, 10000, 10000));
    }


    [ClientRpc]
    public void RpcUpdateSkeletonDefinition()
    {
        Debug.Log("Updating");
        if(OptitrackStreamingClient.FindDefaultClient() != null)
        {
            OptitrackStreamingClient.FindDefaultClient().UpdateDefinitions();
            if (isServer) { 
                foreach(DBPlayerNetworkComponent player in _players)
                {
                    player.ResetSkeletonDefinition();
                }
                Debug.Log("Updated");
            }
            else if (isClient)
            {
                foreach (DBPlayerNetworkComponent player in FindObjectsOfType<DBPlayerNetworkComponent>())
                {
                    player.ResetSkeletonDefinition();
                }
            }
        }
    }

    /// <summary>
    /// Changes <b>Gamestate</b> to Lobby
    /// </summary>
	public void MoveToLobby()
	{
		Debug.Log("MoveToLobby");

        _countdown = _startTimeSeconds;
       
        _gameState = DBGameState.LOBBY;
	}

    /// <summary>
    /// Changes <b>GameState</b> to In_Game. Activiates gameplay logic and timer
    /// </summary>
	public void StartGamePlay()
	{
		Debug.Log("Starting Game");

		_gameState = DBGameState.IN_GAME;

		// Make balls part of game, and reset team state
		int numBalls = _balls.Count;
		for (int i = 0; i < numBalls; ++i)
		{
			_balls[i]._ballState = DBBall.DBBallState.CHARGE00;
			_balls[i]._teamID = DBTeamInfo.TeamID.NONE;
		}

		// Reset teams
		int numTeams = _teamInfos.Count;
		for (int i = 0; i < numTeams; ++i)
		{
			_teamInfos[i].RpcReset();
		}

        if (_conductor != null && !_conductor._isPlaying)
        {

            if (isServer)
            {
                //_glyphGen.RpcGameStart();
            }
            
            _conductor.startMusic();
            _conductor.firstPlay = true;

        }

	}

    /// <summary>
    /// Changes <b>gamestate</b> to End_game, and disables all game logic
    /// </summary>
	public void StopGamePlay()
	{
		Debug.Log("Stopping Game");

		_gameState = DBGameState.END_GAME;

		// Dactivate balls
		int numBalls = _balls.Count;
		for (int i = 0; i < numBalls; ++i)
		{
			_balls[i]._ballState = DBBall.DBBallState.CHARGE00;
			_balls[i]._teamID = DBTeamInfo.TeamID.NONE;
		}

        if (_conductor != null && _conductor._isPlaying)
        {
            if (isServer)
            {
                //_glyphGen.RpcGameStop();
            }
            _conductor.stopMusic();
        }
    }

    /// <summary>
    /// Assigns The closest goal to a client
    /// </summary>
	public void AssignTeams()
	{
		// Go through player list and assign team based on closest goal
		int numPlayers = _players.Count;
		for(int i = 0; i < numPlayers; ++i)
		{
			DBGoal goal = GetClosestGoal(_players[i].transform.position);
			if(goal != null)
			{
				_players[i]._teamID = goal._teamID;
			}
		}
	}

    /// <summary>
    /// gets the closest <b>DBGoal</b> in relation to a given vector position
    /// </summary>
    /// <param name="targetPosition">Target Vector Position</param>
    /// <returns>DBGoal closes to targetPosition</returns>
	public DBGoal GetClosestGoal(Vector3 targetPosition)
	{
		DBGoal closestGoal = null;
		float minDistance = float.MaxValue;
		float tempDistance = 0f;
		int numTeams = _teamInfos.Count;
		for (int i = 0; i < numTeams; ++i)
		{
			tempDistance = (_teamInfos[i]._goal.transform.position - targetPosition).magnitude;
			if (closestGoal == null || tempDistance < minDistance)
			{
				minDistance = tempDistance;
				closestGoal = _teamInfos[i]._goal;
			}
		}
		return closestGoal;
	}

    public void GetPlayerzoneGoal()
    {
        DBGoal[] goals = FindObjectsOfType<DBGoal>();
        for(int i = 0; i < goals.Length; ++i)
        {
            Debug.Log(goals[i]._teamID);
            foreach(DBPlayerNetworkComponent player in goals[i].GetComponentInChildren<DPPlayerVolumeTrigger>()._playersInTrigger)
            {
                player._teamID = goals[i]._teamID;
            }
        }
    }

    // 
    /// <summary>
    /// Called when client has connected
    /// </summary>
    public override void OnStartClient()
	{
		base.OnStartClient();
	}

    /// <summary>
    /// Called when server has started
    /// changes <b>GameState</b> to Lobby
    /// </summary>
    public override void OnStartServer()
	{
		base.OnStartServer();
        Debug.Log("Server Start");
        _players = new List<DBPlayerNetworkComponent>();

		if (isServer)
		{
			if (_gameState == DBGameState.NONE)
			{
				MoveToLobby();
			}
		}
	}
    

    /// <summary>
    /// adds <b>DBPlayer</b> reference to <b>_players</b> list if that player isn't in the list
    /// </summary>
    /// <param name="player"></param>
    public void AddPlayer(DBPlayerNetworkComponent player)
	{
		if(!_players.Contains(player))
		{
			_players.Add(player);
		}
	}

    /// <summary>
    /// Removes a player based on the <b>connectionID</b> of said player
    /// </summary>
    /// <param name="connectionID">ConnectionID of a player</param>
	public void RemovePlayer(int connectionID)
	{
		int numPlayers = _players.Count;
		for(int i = 0; i < numPlayers; ++i)
		{
			if(_players[i].ConnectionID == connectionID)
			{
                Debug.Log("Connection ID: " + connectionID);
				_players.RemoveAt(i);
				break;
			}
		}
	}

	/// <summary>
	/// Returns number of connected players
	/// </summary>
	/// <returns></returns>
	public int GetNumActivePlayers()
	{
		return _players.Count;
	}

	/// <summary>
	/// Returns specific player
	/// </summary>
	/// <param name="index"></param>
	/// <returns></returns>
	public DBPlayerNetworkComponent GetPlayer(int index)
	{
		if(index >= 0 && index < _players.Count)
		{
			return _players[index];
		}
		return null;
	}

    /// <summary>
    /// Gets the first player belonging to a specific team id
    /// </summary>
    /// <param name="teamID">A teams TeamID</param>
    /// <returns>The first player in that team</returns>
	public DBPlayerNetworkComponent GetTeamPlayer(DBTeamInfo.TeamID teamID)
	{
		int numPlayers = _players.Count;
		for (int i = 0; i < numPlayers; ++i)
		{
			if (_players[i]._teamID == teamID)
			{
				return _players[i];
			}
		}
		return null;
	}

    /// <summary>
    /// Gets the specific goal belonging to a team
    /// </summary>
    /// <param name="teamID">The ID of one of the teams</param>
    /// <returns>a reference to the goal object</returns>
	public DBGoal GetTeamGoal(DBTeamInfo.TeamID teamID)
	{
		int numTeams = _teamInfos.Count;
		for (int i = 0; i < numTeams; ++i)
		{
			if (_teamInfos[i]._goal._teamID == teamID)
			{
				return _teamInfos[i]._goal;
			}
		}
		return null;
	}

    /// <summary>
    /// Gets the team id and returns the team info in the list
    /// </summary>
    /// <param name="teamID">The team id</param>
    /// <returns>a reference to to the team's info</returns>
	public DBTeamInfo GetTeamInfo(DBTeamInfo.TeamID teamID)
	{
		int numTeams = _teamInfos.Count;
		for (int i = 0; i < numTeams; ++i)
		{
			if (_teamInfos[i]._teamID == teamID)
			{
				return _teamInfos[i];
			}
		}
		return null;
	}

    /// <summary>
    /// Adds a ball to the list
    /// </summary>
    /// <param name="ball">name of the specific ball object</param>
	public void AddGameBall(DBBall ball)
	{
		if(!_balls.Contains(ball))
		{
			_balls.Add(ball);
		}
	}

    /// <summary>
    /// Removes a ball from the list
    /// </summary>
    /// <param name="ball">name of the specific ball object</param>
	public void RemoveGameBall(DBBall ball)
	{
		_balls.Remove(ball);
	}

    /// <summary>
    /// Sends notifications if a ball touches a goal while it's charged and increments the score for that player
    /// </summary>
    /// <param name="ball">reference to the ball which intersected the goal</param>
    /// <param name="goal">reference to the goal which intersected the ball</param>
	public void NotifyGoal(DBBall ball, DBGoal goal)
	{
		if (isServer)
		{
			// Make sure ball was charged, and on valid team, but not same team
			if (ball._ballState == DBBall.DBBallState.CHARGE01  && ball._teamID != DBTeamInfo.TeamID.NONE && ball._teamID != goal._teamID)
			{
				// Score for team. For now just increment on player. Might want to separate out team info in future.
				DBTeamInfo teamInfo = GetTeamInfo(ball._teamID);
				if(teamInfo != null)
				{
					teamInfo._score += 1;
				}

				// Decharge ball
				ball._ballState = DBBall.DBBallState.CHARGE01;
				ball._teamID = DBTeamInfo.TeamID.NONE;
			}
		}
	}

    /// <summary>
    /// empties the players and balls list, and sets the gamestate to none
    /// </summary>
    public void ResetGameManager()
    {
        _players = new List<DBPlayerNetworkComponent>();
        _balls = new List<DBBall>();
        _gameState = DBGameState.LOBBY;
        if(_gameState == DBGameState.IN_GAME) { 
            StopGamePlay();
            if (isServer)
            {
                //_glyphGen.RpcGameStop();
            }
        }
    }

    /// <summary>
    /// gets the current gamestate
    /// </summary>
    /// <returns>current game state</returns>
    public DBGameState getState()
    {
        return _gameState;
    }
}
