﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NovaGoal : NetworkBehaviour {

    public DBTeamDivider _teamDivider;

    public NovaGoalBrick[] _bricks;
    int i = 0;
            
    [SyncVar]
    public int _netPower = 0;

    [SyncVar]
    public int _netLevel = 0;

    public Vlad.NovaBall _ball;    

	// Use this for initialization
	void Start () {
        _bricks = GetComponentsInChildren<NovaGoalBrick>();
        _ball = FindObjectOfType<Vlad.NovaBall>();
        _teamDivider = FindObjectOfType<DBTeamDivider>();
	}
	
	// Update is called once per frame
	void Update () {

        if(_netPower >= 5)
        {
            _netLevel++;
            _netPower = 0;
            _bricks[i].gameObject.GetComponent<MeshRenderer>().enabled = true;
            i++;
        }
        if(_netPower <= 0)
        {
            _netPower = 0;
        }
        if(_netLevel >= 5)
        {
            _netLevel = 0;
            _teamDivider._phase++;
        }
        if(_netLevel <= 0)
        {
            _netLevel = 0;
        }
		
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col == _ball)
        {
            _netPower -= _ball._ballPower;
            _ball._ballPower = 1;
        }        
    }
}
