﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using DBUtil;

public class PlayerManager : MonoBehaviour {

    public int myNumber;
    public int myScore = 0;

    public GameManager gameManager;
    public Hand[] myHands;

    private void Awake() {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Use this for initialization
    void Start () {
        //gameManager = FindObjectOfType<GameManager>();
        //myNumber = gameManager.players.ToArray().Length;
        //myHands = gameObject.GetComponentsInChildren<Hand>();

        //Required for network local host debugging
        //HMD should be disconnected before testing network functionality via localhost
        //HMD interferes with network debugging
        if (!VRDevice.isPresent)
        {
            Debug.Log("VR Device not found!! Defaulting to Normal Controls!!");
            Camera[] cams = GetComponentsInChildren<Camera>();
            Debug.Log("Num Cams" + cams.Length);
            foreach(Camera c in cams)
            {
                Debug.Log("" + c.name);
                if (c.name.Contains("Anchor") && c.isActiveAndEnabled)
                {
                    c.enabled = false;
                    c.tag = "Untagged";
                    c.gameObject.SetActive(false);
                }
                else if (c.name.Equals("NoHMD_DefaultCam"))
                {
                    c.enabled = true;
                    c.tag = "MainCamera";
                }
                else
                {
                    c.enabled = false;
                }
            }


            
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
