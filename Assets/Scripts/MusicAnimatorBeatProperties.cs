﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MusicAnimatorBeatProperties : MonoBehaviour {


    [HideLabel]
    [BoxGroup("1", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat1Animation;

    [HideInInspector]
    public bool Beat1Dis() { return beat1Animation == AnimationType.None; }
    [HideInInspector]
    public bool Beat1Col() { return beat1Animation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat1Par() { return beat1Animation == AnimationType.Particles; }

    [ShowIf("Beat1Col")]
    [BoxGroup("1")]
    [ColorPalette]
    public Color beat1Color;
    [ShowIf("Beat1Par")]
    [BoxGroup("1")]
    [Range(0.0f, 1.0f)]
    public float beat1ParticleValue;


    [HideLabel]
    [BoxGroup("1 &", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat1AndAnimation;

    [HideInInspector]
    public bool Beat1AndDis() { return beat1AndAnimation == AnimationType.None; }
    [HideInInspector]
    public bool Beat1AndCol() { return beat1AndAnimation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat1AndPar() { return beat1AndAnimation == AnimationType.Particles; }

    [ShowIf("Beat1AndCol")]
    [BoxGroup("1 &")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat1AndColor;
    [ShowIf("Beat1AndPar")]
    [BoxGroup("1 &")]
    [Range(0.0f, 1.0f)]
    public float beat1AndParticleValue;



    [HideLabel]
    [BoxGroup("2", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat2Animation;

    [HideInInspector]
    public bool Beat2Dis() { return beat2Animation == AnimationType.None; }
    [HideInInspector]
    public bool Beat2Col() { return beat2Animation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat2Par() { return beat2Animation == AnimationType.Particles; }

    [ShowIf("Beat2Col")]
    [BoxGroup("2")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat2Color;
    [ShowIf("Beat2Par")]
    [BoxGroup("2")]
    [Range(0.0f, 1.0f)]
    public float beat2ParticleValue;
    

    [HideLabel]
    [BoxGroup("2 &", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat2AndAnimation;

    [HideInInspector]
    public bool Beat2AndDis() { return beat2AndAnimation == AnimationType.None; }
    [HideInInspector]
    public bool Beat2AndCol() { return beat2AndAnimation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat2AndPar() { return beat2AndAnimation == AnimationType.Particles; }

    [ShowIf("Beat2AndCol")]
    [BoxGroup("2 &")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat2AndColor;
    [ShowIf("Beat2AndPar")]
    [BoxGroup("2 &")]
    [Range(0.0f, 1.0f)]
    public float beat2AndParticleValue;


    [HideLabel]
    [BoxGroup("3", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat3Animation;

    [HideInInspector]
    public bool Beat3Dis() { return beat3Animation == AnimationType.None; }
    [HideInInspector]
    public bool Beat3Col() { return beat3Animation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat3Par() { return beat3Animation == AnimationType.Particles; }

    [ShowIf("Beat3Col")]
    [BoxGroup("3")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat3Color;
    [ShowIf("Beat3Par")]
    [BoxGroup("3")]
    [Range(0.0f, 1.0f)]
    public float beat3ParticleValue;


    [HideLabel]
    [BoxGroup("3 &", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat3AndAnimation;

    [HideInInspector]
    public bool Beat3AndDis() { return beat3AndAnimation == AnimationType.None; }
    [HideInInspector]
    public bool Beat3AndCol() { return beat3AndAnimation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat3AndPar() { return beat3AndAnimation == AnimationType.Particles; }

    [ShowIf("Beat3AndCol")]
    [BoxGroup("3 &")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat3AndColor;
    [ShowIf("Beat3AndPar")]
    [BoxGroup("3 &")]
    [Range(0.0f, 1.0f)]
    public float beat3AndParticleValue;


    [HideLabel]
    [BoxGroup("4", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat4Animation;

    [HideInInspector]
    public bool Beat4Dis() { return beat4Animation == AnimationType.None; }
    [HideInInspector]
    public bool Beat4Col() { return beat4Animation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat4Par() { return beat4Animation == AnimationType.Particles; }

    [ShowIf("Beat4Col")]
    [BoxGroup("4")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat4Color;
    [ShowIf("Beat4Par")]
    [BoxGroup("4")]
    [Range(0.0f, 1.0f)]
    public float beat4ParticleValue;


    [HideLabel]
    [BoxGroup("4 &", centerLabel: true)]
    [EnumToggleButtons]
    public AnimationType beat4AndAnimation;

    [HideInInspector]
    public bool Beat4AndDis() { return beat4AndAnimation == AnimationType.None; }
    [HideInInspector]
    public bool Beat4AndCol() { return beat4AndAnimation == AnimationType.Color; }
    [HideInInspector]
    public bool Beat4AndPar() { return beat4AndAnimation == AnimationType.Particles; }

    [ShowIf("Beat4AndCol")]
    [BoxGroup("4 &")]
    [ColorPalette("CyberDodgeBall")]
    public Color beat4AndColor;
    [ShowIf("Beat4AndPar")]
    [BoxGroup("4 &")]
    [Range(0.0f, 1.0f)]
    public float beat4AndParticleValue;

    public bool IsColorEnabled()
    {
        if (Beat1Col() || Beat1AndCol() || Beat2Col() || Beat2AndCol() || Beat3Col() || Beat3AndCol() || Beat4Col() || Beat4AndCol())
            return true;
        else
            return false;
    }
    public bool IsParticlesEnabled()
    {
        if (Beat1Par() || Beat1AndPar() || Beat2Par() || Beat2AndPar() || Beat3Par() || Beat3AndPar() || Beat4Par() || Beat4AndPar())
        return true;
        else
        return false;
    }
}

public enum AnimationType
{
    None,
    Color,
    Particles


}
