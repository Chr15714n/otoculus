﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBGlyphGenerator : NetworkBehaviour
{
	public DBGlyph[] _glyphs;

	private float _beatsPerMin;
	private float _bar;
	private float _measure;

	private enum DBGlyphRequest
	{
		NONE,
		ACTIVE,
		DEACTIVE
	}
	private DBGlyphRequest _pendingGlyphRequest;
	private DBGlyphRequest _glyphState;

	private DBMusicConductor _musicConductor;

	private DBGameManager _gameManager;


	// Use this for initialization
	void Start ()
	{
		if (_musicConductor == null)
		{
			// TODO: Music Conductor should be a singleton
			_musicConductor = GameObject.Find("MusicConductor").GetComponent<DBMusicConductor>();
			if (_musicConductor == null)
			{
				Debug.LogWarning("Unable to find MusicConductor in scene!");
			}
		}

		_pendingGlyphRequest = DBGlyphRequest.NONE;
		_glyphState = DBGlyphRequest.NONE;

		_gameManager = DBGameManager.Singleton;
		_gameManager._notifyGameStateChange += NotifyGameStateChangeCallback;

		DeactivateGlyphs();
	}

	void OnDestroy()
	{
		if(_gameManager != null && DBGameManager.Singleton != null)
		{
			_gameManager._notifyGameStateChange -= NotifyGameStateChangeCallback;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(_gameManager.getState() == DBGameManager.DBGameState.IN_GAME)
		{
			if (isServer)
			{
				if (_pendingGlyphRequest == DBGlyphRequest.ACTIVE)
				{
					_pendingGlyphRequest = DBGlyphRequest.NONE;
					ServerActivateGlyphs();
				}
				else if (_pendingGlyphRequest == DBGlyphRequest.DEACTIVE)
				{
					_pendingGlyphRequest = DBGlyphRequest.NONE;
					RpcDeactivateGlyphs();
				}
			}
		}
	}

	private void OnGUI()
	{
		if (isServer)
		{
			if(_glyphState == DBGlyphRequest.NONE || _glyphState == DBGlyphRequest.DEACTIVE)
			{
				if (GUI.Button(new Rect(300, 10, 100, 60), "Activate\nGlyphs"))
				{
					_pendingGlyphRequest = DBGlyphRequest.ACTIVE;
				}
			}
			else if (_glyphState == DBGlyphRequest.ACTIVE)
			{
				if (GUI.Button(new Rect(300, 10, 100, 60), "Deactivate\nGlyphs"))
				{
					_pendingGlyphRequest = DBGlyphRequest.DEACTIVE;
				}
			}
		}
	}

	private void NotifyGameStateChangeCallback(DBGameManager.DBGameState newState)
	{
		if(newState == DBGameManager.DBGameState.END_GAME)
		{
			if(isServer)
			{
				DeactivateGlyphs();
			}
		}
	}

	public void ServerActivateGlyphs()
	{
		if(_glyphState == DBGlyphRequest.ACTIVE)
		{
			return;
		}

		_glyphState = DBGlyphRequest.ACTIVE;

		int numPlayers = _gameManager.GetNumActivePlayers();
		int numGlyphs = _glyphs.Length;

		// Activate glyph for each player. The glyph will need to be at player's glyph location, and
		// be assigned the player's team.
		for (int i = 0, g = 0; i < numPlayers && g < numGlyphs; ++i)
		{
			DBPlayerNetworkComponent player = _gameManager.GetPlayer(i);
			if(player != null)
			{
				DBGoal goal = _gameManager.GetTeamGoal(player._teamID);
				if(goal != null)
				{
					Vector3 spawnPos = goal._glyphSpawn.transform.position;
					Quaternion spawnRot = goal._glyphSpawn.transform.rotation;
					RpcActivateGlyph(g, spawnPos, spawnRot, player._teamID);
					g++;
				}
			}
		}
	}

	[ClientRpc]
	public void RpcActivateGlyph(int glyphIndex, Vector3 spawnPos, Quaternion spawnRot, DBTeamInfo.TeamID teamID)
	{
		int numGlyphs = _glyphs.Length;
		if(glyphIndex >= 0 && glyphIndex < numGlyphs)
		{
			_glyphs[glyphIndex].ActivateGlyph(spawnPos, spawnRot, teamID);
		}
	}

	[ClientRpc]
	public void RpcDeactivateGlyphs()
	{
		DeactivateGlyphs();
	}

	private void DeactivateGlyphs()
	{
		if(_glyphState == DBGlyphRequest.DEACTIVE)
		{
			return;
		}

		Debug.Log("Deactivate Glyphs");
		_glyphState = DBGlyphRequest.DEACTIVE;

		int numGlyphs = _glyphs.Length;
		for (int i = 0; i < numGlyphs; ++i)
		{
			_glyphs[i].DeactivateGlyph();
		}
	}
}
