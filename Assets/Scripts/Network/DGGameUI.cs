﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class DGGameUI : MonoBehaviour {

    
    public OptitrackStreamingClient _osc;
    public NetworkManager _manager;
    public DBGameManager _dbgm;
    public OptitrackHmd _optiHMD;
    public OptitrackRigidBody _OptiRigid;

    public DBOptitrackSkeletonRealPositionVisualizer _PointVisualizer;

    public bool _usingOptitrack = true;
    private int _nextY = 10;
    private int _nextX = 10;

    private bool _showWindow = false;
    private int _windowID = -1;
    private Rect _windowRect;
    private bool _ingame = false;

    

    [SerializeField]
    string optiServer;
    [SerializeField]
    string optiClient;
    [SerializeField]
    string serverAddress;
    [SerializeField]
    public int hmdnumber = 0;
    [SerializeField]
    public string[] hmdStrings = new string[] { "hmd1", "hmd2", "hmd3", "hmd4" };

    void Awake()
    {
        if (PlayerPrefs.GetString("OptiServer") != null)
            optiServer = PlayerPrefs.GetString("OptiServer");
        if (PlayerPrefs.GetString("OptiClient") != null)
            optiClient = PlayerPrefs.GetString("OptiClient");
        if (PlayerPrefs.GetString("GameServerAddress") != null)
            serverAddress = PlayerPrefs.GetString("GameServerAddress");

        if (_manager == null)
            _manager = GetComponent<NetworkManager>();
        if (_osc == null)
            _osc = OptitrackStreamingClient.FindDefaultClient();
        _osc.ServerAddress = optiServer;
        _osc.LocalAddress = optiClient;
        _manager.networkAddress = serverAddress;
        
    }

    // Use this for initialization
    void Start () {
        _PointVisualizer = FindObjectOfType<DBOptitrackSkeletonRealPositionVisualizer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGUI()
    {
        _nextY = 10;
        _nextX = 10;
        if (!_ingame)
        {
            if (!_usingOptitrack)
            {
                GUI.color = Color.red;
                if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Mo-Cap DISABLED"))
                {
                    _usingOptitrack = true;
                }
                _nextY += 60;
            }
            else
            {
                GUI.color = Color.green;
                if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Mo-Cap ENABLED"))
                {
                    _usingOptitrack = false;
                    if (_osc.enabled) _osc.enabled = false;
                    if (_showWindow) _showWindow = false;
                }
                _nextX += 160;
                GUIStyle style = new GUIStyle();
                style.normal.textColor = _osc.enabled ? Color.green : Color.red;
                
                GUI.Label(new Rect(_nextX, _nextY, 300, 20), "Optitrack Status: " + (_osc.enabled ? "Streaming" : "Disabled"), style);
                _nextY += 22;
                GUI.Label(new Rect(_nextX, _nextY, 300, 20), "Optitrack Server IP: " + optiServer, style);
                _nextY += 22;
                GUI.Label(new Rect(_nextX, _nextY, 300, 20), "Optitrack Client IP:" + optiClient, style);
                
                _nextX = 10;
                _nextY -= 44;
                _nextY += 60;
                GUI.color = Color.white;
                if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Optitrack Status: " + _osc.enabled))
                {
                    _osc.enabled = !_osc.enabled;
                }
                _nextY += 60;
                if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Set Your IP Address"))
                {
                    _windowRect = new Rect(Screen.width / 2, Screen.height / 2, 120, 110);
                    _showWindow = true;
                    _windowID = 0;
                }
                _nextY += 60;
                if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Set IP Address of\nOptitrack Stream" ))
                {
                    _windowRect = new Rect(Screen.width / 2, Screen.height / 2, 120, 110);
                    _showWindow = true;
                    _windowID = 1;
                }
                _nextY += 60;

            }
            GUI.color = Color.white;
            if ((!_usingOptitrack || _osc.enabled) && GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Host Game"))
            { 
                _manager.StartHost();
                _ingame = true;
                _showWindow = false;
            }
            
            _nextY += 60;


            if ((!_usingOptitrack || _osc.enabled) && GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Join Game"))
            {
                _windowRect = new Rect(Screen.width / 2, Screen.height / 2, 220, 250);
                _showWindow = true;
                _windowID = 2;
            }

        }else
        {
            if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Game Options"))
            {
                _dbgm._showingOptions = !_dbgm._showingOptions;
            }
            _nextY += 60;

                if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Player Options"))
                {
                    _windowRect = new Rect(Screen.width / 2, Screen.height / 2, 220, 250);
                    _showWindow = true;
                    _windowID = 3;
                }
                _nextY += 60;

            if (GUI.Button(new Rect(_nextX, _nextY, 150, 50), "Exit"))
            {
                _ingame = false;
                _showWindow = true;
                if (_dbgm._showingOptions) _dbgm._showingOptions = false;
                _manager.StopHost();
            }
        }

        if (_showWindow)
        {
            _windowRect =  GUI.Window(_windowID, _windowRect , SetOptiWindow, "");
        }


    }

    void SetOptiWindow(int windowID)
    {
        switch (windowID)
        {
            case 0:
                
                GUI.Label(new Rect(10, 10, 100, 20), "Your IP Address");
                optiClient = GUI.TextField(new Rect(10, 40, 100, 20), optiClient);
                if (GUI.Button(new Rect(10, 70, 100, 20), "Apply"))
                {
                    PlayerPrefs.SetString("OptiClient", optiClient);
                    _osc.LocalAddress = optiClient;
                    _showWindow = false;
                }  
                break;
            case 1:

                GUI.Label(new Rect(10, 10, 100, 20), "Optitrack IP Address");
                optiServer = GUI.TextField(new Rect(10, 40, 100, 20), optiServer);
                if (GUI.Button(new Rect(10, 70, 100, 20), "Apply"))
                {
                    PlayerPrefs.SetString("OptiServer", optiServer);
                    _osc.ServerAddress = optiServer;
                    _showWindow = false;
                }
                break;
            case 2:
                GUI.Label(new Rect(10, 10, 100, 20), "Join Menu");
                GUI.Label(new Rect(10, 40, 98, 20), "Server IP");
                _manager.networkAddress = serverAddress;
                serverAddress = GUI.TextField(new Rect(100, 40, 98, 20), serverAddress);
                hmdnumber = GUI.SelectionGrid(new Rect(10, 70, 200, 100), hmdnumber, hmdStrings, 2);
                if (GUI.Button(new Rect(10, 180, 200, 60), "Apply"))
                {
                    PlayerPrefs.SetString("GameServerAddress", serverAddress);
                    ((DBNetworkManager)DBNetworkManager.singleton)._clientMotiveRefID = (hmdnumber) + 1;
                    _optiHMD.RigidBodyId = ((hmdnumber + 1) * 10) + 1;
                    _OptiRigid = GetComponentInChildren<OptitrackRigidBody>();
                    if (_OptiRigid != null)
                    {
                        _OptiRigid.RigidBodyId = ((hmdnumber + 1) * 10) + 1;
                    }
                    _manager.StartClient();
                    _ingame = true;
                    _showWindow = false;
                }
                break;
            case 3:

                if (_osc.enabled && _PointVisualizer != null && GUI.Button(new Rect(10, 10, 150, 50), "Apply Points"))
                {
                    if (_PointVisualizer._active)
                    {
                        _PointVisualizer.DestroyPoints();
                        _PointVisualizer._active = false;
                    }
                    else
                    {
                        _PointVisualizer.InitializePoints();
                        _PointVisualizer._active = true;
                    }
                }
                if(GUI.Button(new Rect(10, 62, 150, 50), "ScalePlayer")){

                }
                if (GUI.Button(new Rect(10, 122, 150, 50), "Team Options"))
                {

                }
                if (GUI.Button(new Rect(10, 182, 150, 50), "Close"))
                {
                    _showWindow = false;
                }
                break;

        }

        GUI.DragWindow(new Rect(0, 0, 10000, 10000));

    }
}