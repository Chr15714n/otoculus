﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ParticlePlayground;

public class BallParticleEffects : MonoBehaviour {

    public Rigidbody rb;
    public Vector3 ballVelocity;
    public Transform ballPosition;
    //scr_itemProperties ballProperties;

    public PlaygroundParticlesC part1;
  

    public float myEmissionRate1;
    public float myEmissionRate2;

    //public Transform trans;
    //public Material myMat;
    public Color myColor;

	// Use this for initialization
	void Start () {
       // ballProperties = rb.GetComponent<scr_itemProperties>();

        myEmissionRate1 = part1.emissionRate;
       
        //myMat = trans.GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        float velStuff = Vector3.Magnitude(rb.velocity) / 10;


        Vector3 ballPos = ballPosition.position;

        transform.position = ballPos;
        Debug.Log(velStuff);
        
            ballVelocity = rb.velocity;
            transform.rotation = Quaternion.LookRotation(Vector3.Normalize(rb.velocity), Vector3.up);
        
       
       
            part1.emissionRate = velStuff;
           

           

            
        
       

        //myColor.a = velStuff;
        //myMat.SetColor("_Color", myColor);

    }
}
