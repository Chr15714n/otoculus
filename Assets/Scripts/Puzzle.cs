﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour {

    public Conductor conductor;
    public Net myNet;
    public Floor myTile;
    public Hand[] playerHands;
    public Step[] steps;
    public int i = 0;
    public int prev_i;
    public int stepsComplete = 0;
    
    // Use this for initialization
    void Start () {
        myTile = FindObjectOfType<Floor>();
        myNet = myTile.myNet;
        steps = GetComponentsInChildren<Step>();
        conductor = myTile.conductor;
        playerHands = myTile.myPlayer.myHands;
    }

    void Update () {        

        if (conductor.ticker)
        {            
            steps[i].myBeats[0].material.SetColor("_EmissionColor", Color.red);
            steps[i].myBeats[1].material.SetColor("_EmissionColor", Color.red);
            steps[i].imActive = true;
            i++;
            prev_i = i - 1;            
            conductor.ticker = !conductor.ticker;
        }

        if (i == steps.Length)
        {            
            i = 0;
        }

        if (i == 0)
        {
            foreach (Step step in steps)
            {
                step.myBeats[0].material.SetColor("_EmissionColor", Color.white);
                step.myBeats[1].material.SetColor("_EmissionColor", Color.white);
                step.imActive = false;
                if(stepsComplete > 0)
                {
                    myNet.netPower += stepsComplete;
                    stepsComplete = 0;
                }
            }
            steps[prev_i].myBeats[0].material.SetColor("_EmissionColor", Color.red);
            steps[prev_i].myBeats[1].material.SetColor("_EmissionColor", Color.red);
            steps[prev_i].imActive = true;
        }

        if (i == 1)
        {            
            steps[5].myBeats[0].material.SetColor("_EmissionColor", Color.white);
            steps[5].myBeats[1].material.SetColor("_EmissionColor", Color.white);
            steps[5].imActive = false;
        }
    }    
}
