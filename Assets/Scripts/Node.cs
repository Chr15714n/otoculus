﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

    public GlyphManager GM;
    Renderer rend;
    public Material mat;
    public Color myColor = Color.white;
    public int myNodeNr;
    public bool isActive = false;

	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
        mat = rend.material;
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            //mat.SetColor("_EmissionColor", Color.red);
            GM.nodePower ++;
            GM.ActiveNodes++;
            isActive = true;
        } else
        {
            //rend.material.SetColor("_EmissionColor", myColor);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isActive == true)
        {
            //mat.SetColor("_EmissionColor", Color.red);
        }
        else {
            //mat.SetColor("_EmissionColor", myColor);
        }
    }
}
