﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBOptitrackSkeletonRealPositionVisualizer : MonoBehaviour {

    public Transform[] _bones;
    public GameObject[] _points;
    public bool _active = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (!_active)
            return;
        for (int i = 0; i < _bones.Length; i++)
        {
            _points[i].transform.position = _bones[i].transform.position;
        }
	}

    public void OnGUI()
    {

    }

    public void InitializePoints()
    {
        _bones = GetComponentsInChildren<Transform>();
        _points = new GameObject[_bones.Length];
        for (int i = 0; i < _points.Length; i++)
        {
            _points[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            _points[i].transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
            _points[i].transform.parent = this.transform;
        }
    }
    public void DestroyPoints()
    {
        for (int i = 0; i < _points.Length; i++)
        {
            Destroy(_points[i]);
        }
    }
}
