﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class DBPlayerNetworkComponent : NetworkBehaviour
{



    //Determines what team to change a player to with the CmdTeamChange Function
    private int _teamSwitchControl = 0;

    //Motive IDs
    [SyncVar(hook = "OnMotiveReferenceIDChange")]
    public int _motiveReferenceID;
    [SyncVar(hook = "OnMotiveBodyIDChange")]
    public int _motiveRigidBodyID;
    [SyncVar(hook = "OnMotiveSkeletonIDChange")]
    public string _motiveSkeletonName;

    //Motive Object References
    private OptitrackStreamingClient _optitrackStream;
    public OptitrackRigidBody _optitrackBody;
    public OptitrackSkeletonAnimator _optitrackSkeleton;

    //Player Score
    private int _playerScore;
    public int Score { get { return _playerScore; } set { _playerScore = value; } }

    //Connection ID for the network
    public int ConnectionID;

    //Team Value
    [SyncVar(hook = "OnTeamChange")]
    public DBTeamInfo.TeamID _teamID;

    //Hand and Body Triggers
    public DBTriggerComponent _bodyTrigger;
    public DBTriggerComponent _handTriggerLeft;
    public DBTriggerComponent _handTriggerRight;

    

    // Use this for initialization
    void Start()
    {
        Debug.Log("Start Player");
        
        //Setting optitrack if steaming client enabled
        _optitrackStream = OptitrackStreamingClient.FindDefaultClient();
        if (_optitrackStream.enabled)
        {
            //assigns optitrack streaming client and does all below if enabled
            Debug.Log("New ID: " + OptitrackStreamingClient.FindDefaultClient().LocalAddress);

            Debug.Log("Motive ID" + ((DBNetworkManager)DBNetworkManager.singleton)._clientMotiveRefID);
            if (_optitrackBody != null)
            {
                _optitrackSkeleton.StreamingClient = ((DBNetworkManager)DBNetworkManager.singleton)._optitrackStream;
                _optitrackBody.RigidBodyId = _motiveRigidBodyID;
            }
            if (_optitrackSkeleton != null && (_motiveSkeletonName != null && _motiveSkeletonName.Length > 0))
            {
                _optitrackSkeleton.StreamingClient = OptitrackStreamingClient.FindDefaultClient();
                Debug.Log("Skeleton Name: " + _motiveSkeletonName);
                _optitrackSkeleton.SkeletonAssetName = _motiveSkeletonName;
                if (!_optitrackSkeleton.isActiveAndEnabled)
                {
                    _optitrackSkeleton.enabled = true;
                }
            }
            if (isLocalPlayer)
            {
                CmdSendMotiveReferenceID(((DBNetworkManager)DBNetworkManager.singleton)._clientMotiveRefID);
            }
        }

        _bodyTrigger._parentGameObject = this.gameObject;
        _bodyTrigger._notifyOnTriggerEnter = NotifyOnTriggerEnter;
        _bodyTrigger._notifyOnTriggerStay = NotifyOnTriggerStay;
        _bodyTrigger._notifyOnTriggerExit = NotifyOnTriggerExit;
    }


    private void OnGUI()
    {
        if (isLocalPlayer)
        {
            // Show team for client
            GUI.Label(new Rect(Screen.width - 100, 100, 100, 60), "Team\n" + _teamID);

            if (GUI.Button(new Rect(160, 10, 150, 50), "Switch Team"))
            {
                CmdChangeTeam();
            }
        }
    }

    //Resets the skeleton in case any changes are made in motive
    public void ResetSkeletonDefinition()
    {
        //_optitrackSkeleton.m_skeletonDef.Id = OptitrackStreamingClient.FindDefaultClient().GetSkeletonDefinitionByName(_optitrackSkeleton.SkeletonAssetName).Id;
    }

    [System.Obsolete("Function no longer used, please review code", false)]
    public void CompleteBallCharge(DBBall ball)
    {
        //ball._teamID = _teamID;
        //ball._ballState = DBBall.DBBallState.CHARGED;
    }

    /// <summary>
    /// Checks if any part of a player has entered a trigger
    /// </summary>
    /// <param name="triggerComponent">DBTriggerComponent</param>
    /// <param name="other">The collider of the other object</param>
    void NotifyOnTriggerEnter(DBTriggerComponent triggerComponent, Collider other)
    {
        if (isServer)
        {
            
        }
    }

    /// <summary>
    /// Checks if any part of a player is in a trigger
    /// </summary>
    /// <param name="triggerComponent">DBTriggerComponent</param>
    /// <param name="other">The collider of the other object</param>
    void NotifyOnTriggerStay(DBTriggerComponent triggerComponent, Collider other)
    {
        if (isServer)
        {

        }
    }
    /// <summary>
    /// Checks if any part of a player has exited a trigger
    /// </summary>
    /// <param name="triggerComponent">DBTriggerComponent</param>
    /// <param name="other">The collider of the other object</param>
    void NotifyOnTriggerExit(DBTriggerComponent triggerComponent, Collider other)
    {
        if (isServer)
        {

        }
    }

    //Does logic to all clients over the network for when the player switches teams
    public void OnTeamChange(DBTeamInfo.TeamID teamID)
    {
        _teamID = teamID;
        Color teamColor = DBTeamInfo.GetTeamColor(_teamID);
        //if skinned mesh renderer is available, change the material color on that, otherwise change the material on the default player object
        if (GetComponentInChildren<SkinnedMeshRenderer>() != null)
        {
            GetComponentInChildren<SkinnedMeshRenderer>().material.color = teamColor;
        }
        else
        {
            GetComponentInChildren<MeshRenderer>().material.color = teamColor;
        }
    }

    //motive id hook
    void OnMotiveReferenceIDChange(int id)
    {
        _motiveReferenceID = id;
    }
    //motive rigid body id hook
    void OnMotiveBodyIDChange(int id)
    {
        _motiveRigidBodyID = id;
        if (_optitrackBody != null)
        {
            _optitrackBody.RigidBodyId = _motiveRigidBodyID;
        }
    }

    //motive skeleton id hook
    void OnMotiveSkeletonIDChange(string id)
    {
        _motiveSkeletonName = id;
        if (_optitrackSkeleton != null)
        {
            _optitrackSkeleton.SkeletonAssetName = _motiveSkeletonName;
            if (!_optitrackSkeleton.isActiveAndEnabled)
            {
                _optitrackSkeleton.enabled = true;
            }
        }
    }

    //sends reference id to server to be applied to other player models
    [Command]
    public void CmdSendMotiveReferenceID(int id)
    {
        if (isServer)
        {
            _motiveRigidBodyID = ((DBNetworkManager)DBNetworkManager.singleton).GetMotiveID(id);
            _motiveRigidBodyID = (_motiveRigidBodyID * 10) + 1;
            _motiveSkeletonName = "hmd" + id;
        }
    }

    //logic for team change
    [Command]
    public void CmdChangeTeam()
    {
        switch (_teamSwitchControl)
        {
            case 0: _teamID = DBTeamInfo.TeamID.BLUE; break;
            case 1: _teamID = DBTeamInfo.TeamID.GREEN; break;
            case 2: _teamID = DBTeamInfo.TeamID.RED; break;
            case 3: _teamID = DBTeamInfo.TeamID.YELLOW; break;
        }
        _teamSwitchControl++;
        if (_teamSwitchControl > 3)
        {
            _teamSwitchControl = 0;
        }
    }
}