﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ParticlePlayground;
using Sirenix.OdinInspector;

namespace Morgan {
    namespace fx {
        /*------------------------------------------------- Ball Follow -----
         | 
         |  This class will rotate the membrane to always be facing in the 
         |  direction the ball is travelling
         |
         |  Use Lerp Speed to effect how quickly the membrane re-orients to
         |  it's new position
         *-------------------------------------------------------------------*/

        public class BallFollow : MonoBehaviour {
            [BoxGroup("Actions")]
            public bool matchLocation;
            [BoxGroup("Actions")]
            public bool lerpToLocation;
            [BoxGroup("Actions")]
            public bool lerpRotation;
            [BoxGroup("Target Transform")]
            public Transform target;


            [BoxGroup("Lerp Settings")]
            public float lerpSpeed;


            Rigidbody ballRb;
            Quaternion nextRotation;

            void Start() {
                //target = GameObject.Find("Ball").transform;
                if (target.gameObject.name == "Ball")
                    ballRb = target.GetComponent<Rigidbody>();
            }

            void Update() {
                if (target != null) {
                    if (matchLocation) { transform.position = target.position; }

                    if (lerpToLocation) {
                        transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * lerpSpeed);

                    }

                    if (lerpRotation && ballRb) {
                        nextRotation = Quaternion.LookRotation(Vector3.Normalize(ballRb.velocity), Vector3.up);
                        transform.localRotation = Quaternion.Lerp(transform.localRotation, nextRotation, Time.deltaTime * lerpSpeed);

                    }
                }
                else {
                    if (FindObjectsOfType<Vlad.NovaBall>().Length > 0) {
                        target = FindObjectOfType<Vlad.NovaBall>().transform;
                    }
                }
            }
        }
    }
}


