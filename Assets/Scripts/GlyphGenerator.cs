﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GlyphGenerator : NetworkBehaviour
{

    //public GameObject ball;//array/list
    public GameObject[] glyphs;
    public GameObject currentGlyph;
    public List<GlyphManager> _glyphManagers;
    public DBGameManager _dbgm;
    public DBBall[] balls;
    //public GameObject levelMusic;
    int index;
    
    public float BPM;

    [SyncVar (hook = "OnBeatChange")]
    public int Beat = 0;

    public float Bar = 0;
    public float Measure = 0;
    public bool needGlyph = false;
    public bool killMe = false;
    private bool _isGameStatePlaying = false;

    public DBMusicConductor _conductor;


    // Use this for initialization
    void Start()
    {
        //ball = GameObject.Find("Ball");
        //levelMusic = GameObject.Find("LevelMusic");
        needGlyph = false;
        //StartCoroutine("PuzzlePlay");

        _conductor = GameObject.Find("MusicConductor").GetComponent<DBMusicConductor>();

    }

    private void OnGUI()
    {
        if (isServer) { 
            if (GUI.Button(new Rect(300, 10, 100, 60), "Summon Glyph"))
            {
                needGlyph = true;
                index = Random.Range(0, glyphs.Length);
                RpcNextGlyph(index);
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        Beat = _conductor.currentBeats - 1;
        //RpcBeatUpdate();


        if (needGlyph && _isGameStatePlaying)
        {

            
            if (hasAuthority) {
                index = Random.Range(0, glyphs.Length);
                RpcNextGlyph(index);
            }

        }



        if (Bar == 4)
        {
            //needGlyph = true;
            Measure++;
            Bar = 0;
        }

        if (Measure == 4)
        {
            Measure = 0;
        }
    }

    void OnBeatChange(int newbeat)
    {
        Beat = newbeat;
    }

    [ClientRpc]
    public void RpcSpawnGlyph(int i, DBTeamInfo.TeamID teamid)
    {

        currentGlyph = glyphs[i];
        _glyphManagers.Add(currentGlyph.GetComponent<GlyphManager>());
        GameObject.Instantiate(currentGlyph);

        //GM = currentGlyph.GetComponent<GlyphManager>();
        needGlyph = false;

    }

    [ClientRpc]
    public void RpcBeatUpdate()
    {
        Beat = _conductor.currentBeats - 1;
    }

    [ClientRpc]
    public void RpcNextGlyph(int i)
    {
        
        currentGlyph = glyphs[i];
        GameObject.Instantiate(currentGlyph);
        //GM = currentGlyph.GetComponent<GlyphManager>();
        needGlyph = false;

    }

    [ClientRpc]
    public void RpcGameStart()
    {
        _isGameStatePlaying = true;
        //StartCoroutine("PuzzlePlay");
        Debug.Log("Coroutine Started");
    }

    [ClientRpc]
    public void RpcGameStop()
    {
        _isGameStatePlaying = false;
        //StopCoroutine("PuzzlePlay");
        Debug.Log("Coroutine Stopped");
    }


    /*
    private IEnumerator PuzzlePlay()
    {
        //levelMusic.GetComponent<AudioSource>().Play();
        while (true)
        {
            //Beat++;
            yield return new WaitForSeconds(60 / BPM);
           // Beat++;
            yield return new WaitForSeconds(60 / BPM);
            //Beat++;
            yield return new WaitForSeconds(60 / BPM);
           // Beat++;
            yield return new WaitForSeconds(60 / BPM);
           // Beat = 0;
            Bar++;
        }
    }
    */




}
