﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Morgan.Networking;

namespace Vlad {
    namespace Networking {
        public class NetworkGameManager : NetworkManager {

            public List<GameObject> players = new List<GameObject>();

            //prefabs for what the net manager should spawn
            public GameObject _activeBallPrefab;
            public GameObject _heldBallPrefab;

            // currently active objects - One should be null while the other is active
            private GameObject activeBall;
            private GameObject heldBall;

            // Use this for initialization
            void Start() {

            }

            // Update is called once per frame
            void Update() {

            }

            /**Overridding Network Functions**/

            public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
                /**
                 * Default implementation 
                 */
                var player = (GameObject)GameObject.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
                NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
                print("Server Added Player: ");
                print(conn.connectionId);
                print(conn.address);
                player.GetComponent<NovaHandsV2>()._uniqueID = conn.connectionId;
                players.Add(player);

                /**
                 * Additional implementation
                 */
                RemoveNullPlayersFromList();
            }

            private void OnPlayerConnected(NetworkPlayer player) {
                Debug.Log("PLAYER CONNECTED INVOKED: " + player.ToString());
                RemoveNullPlayersFromList();

            }

            private void OnPlayerDisconnected(NetworkPlayer player) {
                Debug.Log("PLAYER DISCONNECT INVOKED");
                RemoveNullPlayersFromList();
            }

            public override void OnServerDisconnect(NetworkConnection conn) {
                base.OnServerDisconnect(conn);
                Debug.Log("SERVER DISCONNECT INVOKED");
                RemoveNullPlayersFromList();

            }

            public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player) {
                players.Remove(player.gameObject);
                RemoveNullPlayersFromList();
                base.OnServerRemovePlayer(conn, player);
                Debug.Log("OnServerRemovePlayer INVOKED");


            }



            public override void OnClientDisconnect(NetworkConnection conn) {
                base.OnClientDisconnect(conn);
                Debug.Log("OnClientDisconnect INVOKED");
            }

            public override void OnStartClient(NetworkClient client) {
                base.OnStartClient(client);

            }

            /*Other Function Implementation*/
            public void RemoveNullPlayersFromList() {
                foreach (var pl in players) {
                    if (pl == null) {
                        players.Remove(pl);
                        RemoveNullPlayersFromList();
                        return;
                    }
                }
            }

            

            /**/
        }//end class
    }//end namespace 1
}//end namespace 2