﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Vlad.Networking;
using UnityEngine.VR;
using UnityStandardAssets.CrossPlatformInput;

public class NovaPlayerController : NetworkBehaviour {

    //network stuff
    public Transform NetworkPlayer;
    private bool serverconnected;

    //Rotation Controls
    public float rotationSpeed = 7.5f;
    public float dampingTime = 0.2f;

    private Vector3 m_TargetAngles;
    private Vector3 m_FollowAngles;
    private Vector3 m_FollowVelocity;
    private Quaternion m_OriginalRotation;

    //movement controls

    float moveSpeed = 0.051f;

    void Start() {
        DontDestroyOnLoad(this);

        m_OriginalRotation = transform.localRotation;
        if (VRDevice.isPresent) {
            print("Starting Local Player with VR Device");
            this.transform.position = new Vector3(0, 0, 0);
        }
        else {
            print("Starting Local Player without VR Device");
            this.transform.position = new Vector3(0, 1, 0);
        }
    }

    // Update is called once per frame
    void Update() {

        if (NetworkPlayer != null) {
            //rotational values
            float inputH = 0;
            float inputV = 0;
            float inputX = 0;
            float inputZ = 0;
            if (VRDevice.isPresent) {
                inputH = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x / 10;
                //inputV = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).y;

                inputX = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).x / 2;
                inputZ = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).y / 2;
                //print("XZ" + inputX + ":" + inputZ);
            }
            else {
                //mouse rotation
                inputH = CrossPlatformInputManager.GetAxis("Mouse X");
                inputV = CrossPlatformInputManager.GetAxis("Mouse Y");


                inputZ = CrossPlatformInputManager.GetAxis("Vertical");
                inputX = CrossPlatformInputManager.GetAxis("Horizontal");
            }
            // wrap values to avoid springing quickly the wrong way from positive to negative
            if (m_TargetAngles.y > 180) {
                m_TargetAngles.y -= 360;
                m_FollowAngles.y -= 360;
            }
            if (m_TargetAngles.x > 180) {
                m_TargetAngles.x -= 360;
                m_FollowAngles.x -= 360;
            }
            if (m_TargetAngles.y < -180) {
                m_TargetAngles.y += 360;
                m_FollowAngles.y += 360;
            }
            if (m_TargetAngles.x < -180) {
                m_TargetAngles.x += 360;
                m_FollowAngles.x += 360;
            }

            m_TargetAngles.y += inputH * rotationSpeed;
            m_TargetAngles.x += inputV * rotationSpeed;

            //m_TargetAngles.y = Mathf.Clamp(m_TargetAngles.y, -rotationRange.y * 0.5f, rotationRange.y * 0.5f);
            m_TargetAngles.x = Mathf.Clamp(m_TargetAngles.x, -75.0f * 0.5f, 75.0f * 0.5f);

            // smoothly interpolate current values to target angles
            m_FollowAngles = Vector3.SmoothDamp(m_FollowAngles, m_TargetAngles, ref m_FollowVelocity, dampingTime);

            // update the actual gameobject's rotation
            transform.localRotation = m_OriginalRotation * Quaternion.Euler(-m_FollowAngles.x, m_FollowAngles.y, 0);

            //movement
            this.transform.position += new Vector3(this.transform.forward.x * inputZ * (moveSpeed), 0, this.transform.forward.z * inputZ * (moveSpeed));
            this.transform.position += new Vector3(this.transform.right.x * inputX * (moveSpeed), 0, this.transform.right.z * inputX * (moveSpeed));
        }
        else {
            foreach (NetworkPlayerManager player in FindObjectsOfType<NetworkPlayerManager>()) {
                if (player.isLocalPlayer) {
                    NetworkPlayer = player.transform;
                    return;
                }
            }
        }
    }

    private void OnConnectedToServer() {
        if (!isLocalPlayer) {
            Destroy(this);
        }
    }

    private void OnDisconnectedFromServer(NetworkDisconnection info) {

    }

    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();

    }
}