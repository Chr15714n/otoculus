﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step : MonoBehaviour
{

    public Puzzle myPuzzle;
    public Renderer[] myBeats;
    public bool imActive = false;
    public SphereCollider[] spheres;
    public GestHit[] gestHit;
    public Hand[] hands;
    public int hitsComplete = 0;

    // Use this for initialization
    void Start()
    {
        gestHit = GetComponentsInChildren<GestHit>();
        hands = FindObjectsOfType<Hand>();
        myPuzzle = GetComponentInParent<Puzzle>();
        myBeats = GetComponentsInChildren<Renderer>();
        spheres = GetComponentsInChildren<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(hitsComplete == 2)
        {
            myPuzzle.stepsComplete++;
        }     

        if (imActive)
        {
            foreach (SphereCollider sphere in spheres)
            {
                sphere.enabled = true;
            }
        }

        if (!imActive)
        {
            foreach (SphereCollider sphere in spheres)
            {
                sphere.enabled = false;
            }
        }
    }
}
