﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource))]
public class DBNetworkAudioController : NetworkBehaviour {

    private AudioSource _source;

    public AudioClip[] _musicCollection;

	// Use this for initialization
	void Start () {
        _source = this.GetComponent<AudioSource>();
	}
	
    public void PlayMusic(int musicCollectionIndexValue)
    {

    }

    

    [ClientRpc]
    void RpcSendMusicToClients(int musicCollectionIndexValue)
    {
        _source.PlayOneShot(_musicCollection[musicCollectionIndexValue]);
        _source.loop = true;
    }

}
