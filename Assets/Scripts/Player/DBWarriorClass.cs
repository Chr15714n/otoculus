﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBWarriorClass : MonoBehaviour {

    public int _myPowerLevel;
    public bool _caught;
    public DBBall _ball;
    public DBGoal _net;
    public DBGameManager _gameManager;
    public DBTriggerComponent _myPlayerTrigger;

    public GameObject _myPlayer;  

    // Use this for initialization
    void Start () {
        _caught = false;
        _myPowerLevel = 0;        
        
        _myPlayer = gameObject;
        _gameManager = FindObjectOfType<DBGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if (_myPlayerTrigger._notifyOnTriggerEnter != null)
        {
            Catch();
            //TO-DO Uncatch
        }

        if(_myPlayerTrigger._notifyOnTriggerExit != null)
        {
            _caught = false;
        }

        if(_myPowerLevel <= 5)
        {
            _myPowerLevel = 5;
        }

        if(_myPowerLevel >= 0)
        {
            _myPowerLevel = 0;
        }
        
        if(_gameManager._gameState == DBGameManager.DBGameState.IN_GAME)
        {
            if (_ball == null)
            {
                _ball = FindObjectOfType<DBBall>();
                if (_net == null)
                {
                    _net = FindObjectOfType<DBGoal>();
                    if (_myPlayerTrigger == null)
                    {
                        _myPlayerTrigger = GetComponentInChildren<DBTriggerComponent>();
                    }
                }
            }
        }
	}

    public void Catch()
    {
        if (_caught == false)
        {
            _myPowerLevel += (int)_ball._ballState;
            _caught = true;
        }
    }

    public void Hit()
    {
        if (_caught == false)
        {
            _myPowerLevel -= (int)_ball._ballState;
        }
    }
}
