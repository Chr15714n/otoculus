﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestHit : MonoBehaviour {

    public Step myStep;

	// Use this for initialization
	void Start () {
        myStep = GetComponentInParent<Step>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Hand>() && myStep.imActive)
        {
            myStep.hitsComplete++;
        }
    }
}
