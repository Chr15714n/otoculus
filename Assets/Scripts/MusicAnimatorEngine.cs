﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using ParticlePlayground;

[RequireComponent(typeof(MusicAnimatorBeatProperties))]
[ExecuteInEditMode]
public class MusicAnimatorEngine : MonoBehaviour
{
    [Button]
    private void UpdateInspector()
    {
        InspectorUpdate();
    }

    MusicAnimatorBeatProperties mabp {
        get
        {
            return GetComponent<MusicAnimatorBeatProperties>();
        }
    }
        
    Conductor conductor;

    [BoxGroup("Animator Options")]
    public bool linkToAnAnimator;

    [InfoBox("Please attach a Animator to this GameObject", InfoMessageType.Warning, "ShowAnimatorWarning")]
    [ShowIf("linkToAnAnimator")]
    [BoxGroup("Animator Options")]
    public bool useMyAnimator = true;

    [ShowIf("linkToAnAnimator")]
    [HideIf("useMyAnimator")]
    [BoxGroup("Animator Options")]
    [LabelText("LINK TO AN ANIMATOR")]
    public Animator animator;

    [InfoBox("Please Attach this component to an already existing Particle Playground System, or do not select 'UseMyParticle', and direct this script to a Particle system. Do not add one to this GameObject, as it causes indexing error with Particle Playground.", InfoMessageType.Error, "ShowParticleWarning")]

    [ShowIf("usingParticles")]
    [BoxGroup("Particle Animation")]
    public bool useMyParticles = true;

    [ShowIf("usingParticles")]
    [HideIf("useMyParticles")]
    [BoxGroup("Particle Animation")]
    [LabelText("LINK TO PARTICLE SYSTEM")]
    public PlaygroundParticlesC particles;


    [InfoBox("Please attach a MeshRenderer to this GameObject", InfoMessageType.Warning, "ShowColorWarning")]
    [ShowIf("usingColor")]
    [BoxGroup("Color Animation")]
    public bool animateMe = true;


    [ShowIf("usingColor")]
    [HideIf("animateMe")]
    [BoxGroup("Color Animation")]
    public MeshRenderer meshRendererToAnimate;


    [ShowIf("usingColor")]
    [HideIf("multipleMaterials")]
    [BoxGroup("Color Animation")]
    public Material material;
   /* {
        get
        {
            if (Application.isPlaying)
            {
                if (multipleMaterials && meshRendererToAnimate != null)
                {
                    return meshRendererToAnimate.materials[materialIndex];
                }
                else if (!multipleMaterials && meshRendererToAnimate != null)
                    return meshRendererToAnimate.material;
                else return null;
            }

            else
            {
                if (multipleMaterials && meshRendererToAnimate)
                {
                    return meshRendererToAnimate.sharedMaterials[materialIndex];

                }
                else if (!multipleMaterials && meshRendererToAnimate)
                    return meshRendererToAnimate.sharedMaterial;
                else return null;
            }
            
        }
       
    }*/

    [ShowIf("multipleMaterials")]
    [BoxGroup("Color Animation")]
    public int materialIndex = 0;

    [ShowIf("usingColor")]
    [ShowIf("multipleMaterials")]
    [BoxGroup("Color Animation")]
    public Material[] materials
    {
        get
        {
            if (multipleMaterials && meshRendererToAnimate != null)
            {
                return meshRendererToAnimate.GetComponent<MeshRenderer>().sharedMaterials;
            }
            else return new Material[1];
        }
    }

    public bool multipleMaterials
    {
        get
        {
            if (meshRendererToAnimate != null)
                return meshRendererToAnimate.sharedMaterials.Length > 1;
            else return false;
        }
    }




    [ReadOnly]
    [ShowIf("usingColor")]
    [BoxGroup("Beat Properties")]
    public Color[] colorValues = new Color[8];
    [ReadOnly]
    [ShowIf("usingParticles")]
    [BoxGroup("Beat Properties")]
    public float[] particleValues = new float[8];
    [HideInInspector]
    public AnimationType[] animationTypes = new AnimationType[8];

    
    private void Start()
    {
        UpdateInspector();
         if (Application.isPlaying)
        {
            if (multipleMaterials && usingColor)
                material = meshRendererToAnimate.materials[materialIndex];
            else if (!multipleMaterials && usingColor)
                material = meshRendererToAnimate.material;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!Application.isPlaying) return;

        //If animated, Sync the animator to the beats of the conductor
        if (linkToAnAnimator && animator)
        {
            animator.SetInteger("Beat", conductor.currentBeats);
        }

        DoStuffOnBeats(conductor.currentBeats, animationTypes, colorValues, particleValues);

    }

    public void DoStuffOnBeats(int beat, AnimationType[] animType, Color[] color, float[] emissionRate)
    {
        if (beat == 0) beat = 1;
        if (animType[beat-1] == AnimationType.Color && material)
        {
            material.SetColor("_EmissionColor", color[beat - 1]);
        }
        else if (animType[beat-1] == AnimationType.Particles && particles)
        {
            particles.emissionRate = emissionRate[beat - 1];
        }
        else
        {
            return;
        }
    }

    //UPDATES TO ASSIGN COMPONENTS AND REPOPULATE ANIMATION LISTS ------------------------
    
    
    void InspectorUpdate()
    {
        //mabp = GetComponent<MusicAnimatorBeatProperties>();
        conductor = GameObject.Find("Conductor").GetComponent<Conductor>();

        if (usingColor && animateMe && GetComponent<MeshRenderer>())
        {
            meshRendererToAnimate = GetComponent<MeshRenderer>();
        }
        else
        {
            meshRendererToAnimate = null;
        }

        if (useMyAnimator && GetComponent<Animator>())
            animator = GetComponent<Animator>();
        else
        {
            animator = null;
        }

        if (usingParticles && useMyParticles && GetComponent<PlaygroundParticlesC>())
        {
            particles = GetComponent<PlaygroundParticlesC>();
        }
        else
        {
            particles = null;
        }

        ValuesUpdate();
            
       

    }

    void ValuesUpdate()
    { 
        
        particleValues[0] = mabp.beat1ParticleValue;
        colorValues[0] = mabp.beat1Color;
        animationTypes[0] = mabp.beat1Animation;

        particleValues[1] = mabp.beat1AndParticleValue;
        colorValues[1] = mabp.beat1AndColor;
        animationTypes[1] = mabp.beat1AndAnimation;

        particleValues[2] = mabp.beat2ParticleValue;
        colorValues[2] = mabp.beat2Color;
        animationTypes[2] = mabp.beat2Animation;

        particleValues[3] = mabp.beat2AndParticleValue;
        colorValues[3] = mabp.beat2AndColor;
        animationTypes[3] = mabp.beat2AndAnimation;

        particleValues[4] = mabp.beat3ParticleValue;
        colorValues[4] = mabp.beat3Color;
        animationTypes[4] = mabp.beat3Animation;

        particleValues[5] = mabp.beat3AndParticleValue;
        colorValues[5] = mabp.beat3AndColor;
        animationTypes[5] = mabp.beat3AndAnimation;

        particleValues[6] = mabp.beat4ParticleValue;
        colorValues[6] = mabp.beat4Color;
        animationTypes[6] = mabp.beat4Animation;

        particleValues[7] = mabp.beat4AndParticleValue;
        colorValues[7] = mabp.beat4AndColor;
        animationTypes[7] = mabp.beat4AndAnimation;

        for (int i =0; i < animationTypes.Length; i++)
        {
            ArrayResetter(i, colorValues, particleValues);
        }



    }

    public void ArrayResetter(int num, Color[] color, float[] partvalues)
    {
        if (animationTypes[num] != AnimationType.Color)
        {
            color[num] = Color.black;
        }

        if (animationTypes[num] != AnimationType.Particles)
        {
            partvalues[num] = 0;
        }
    }

    //BOOLS FOR INSPECTOR STUFF-------------------------------------------

    public bool usingColor
    {
        get
        {
            return mabp.IsColorEnabled();
        }
    }

    public bool usingParticles
    {
        get
        {
            return mabp.IsParticlesEnabled();
        }
    }

    [HideInInspector]
    public bool ShowColorWarning
    {
        get
        {
            if (animateMe && GetComponent<MeshRenderer>())
                return false;
            else if (animateMe && !GetComponent<MeshRenderer>())
                return true;
            else
                return false;
        }
    }

    [HideInInspector]
    public bool ShowAnimatorWarning
    {
        get
        {
            if (useMyAnimator && GetComponent<Animator>())
                return false;
            else if (useMyAnimator && !GetComponent<Animator>())
                return true;
            else
                return false;
        }
    }

    [HideInInspector]
    public bool ShowParticleWarning
    {
        get
        {
            if (useMyParticles && GetComponent<PlaygroundParticlesC>())
                return false;
            else if (useMyParticles && !GetComponent<PlaygroundParticlesC>())
                return true;
            else
                return false;
        }
    }

}
