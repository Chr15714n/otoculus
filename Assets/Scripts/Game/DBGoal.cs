﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBGoal : NetworkBehaviour
{
	public DBTeamInfo.TeamID _teamID;

	public TextMesh _scoreText;

	// Child trigger object that will get the events from physics and will foward to us via delegates
	public DBTriggerComponent _triggerComponent;

	public GameObject _glyphSpawn;
    public DBBall _ball;

    public int _netPower = 0;

	void Start()
	{
        _ball = FindObjectOfType<DBBall>();
		_triggerComponent._parentGameObject = this.gameObject;
		_triggerComponent._notifyOnTriggerEnter = NotifyOnTriggerEnter;
		_triggerComponent._notifyOnTriggerExit = NotifyOnTriggerExit;
	}


	void NotifyOnTriggerEnter(DBTriggerComponent triggerComponent, Collider other)
	{
        if (other.GetComponent<DBBall>())
        {
            _netPower -= (int)_ball._ballState;
        }
    }

	void NotifyOnTriggerExit(DBTriggerComponent triggerComponent, Collider other)
	{

	}

}
