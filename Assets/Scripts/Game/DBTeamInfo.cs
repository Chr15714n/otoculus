﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBTeamInfo : NetworkBehaviour
{

	public enum TeamID
	{
		NONE,
		RED,
		BLUE,
		GREEN,
		YELLOW
	}

	public TeamID _teamID;

	public static Color[] TeamColors = { Color.white, Color.red, Color.blue, Color.green, Color.yellow };

	public DBGoal _goal;

	[SyncVar(hook = "OnScoreChange")]
	public int _score;

	[ClientRpc]
	public void RpcReset()
	{
		Debug.Log("Resetting DBTeamInfo");
		_score = 0;
	}    

	public static Color GetTeamColor(DBTeamInfo.TeamID teamID)
	{
		int teamIndex = (int)teamID;
		if (teamIndex >= 0 && teamIndex < TeamColors.Length)
		{
			return TeamColors[teamIndex];
		}
		return Color.white;
	}

	public void OnScoreChange(int newScore)
	{
		_score = newScore;

		// Update goal score
		DBGoal goal = DBGameManager.Singleton.GetTeamGoal(_teamID);
		if(goal != null)
		{
			goal._scoreText.text = _score.ToString();
		}
	}
}