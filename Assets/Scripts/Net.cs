﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Net : MonoBehaviour {

    public Floor myTile;
    public BeatPointManager beatPoint;
    public GlyphPiece[] glyphPieces;

    public Ball ball;

    public int netPower = 0;
    public int netTotal = 0;
    int i = 0;

	// Use this for initialization
	void Start () {
        
        ball = FindObjectOfType<Ball>();
    }
	
	// Update is called once per frame
	void Update () {
        if (myTile != null)
        beatPoint = myTile.myBeatPoint;
                
        if (netPower >= 5)
        {
            netPower = 0;
            glyphPieces[i].gameObject.SetActive(true);
            i++;
            beatPoint.powerUp = false;
        }
        //TODO Create score clamping to preserve milestones   
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<Ball>())
        {
            netPower = 0;
            netTotal -= ball.myChargeLevel + 1;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Floor>())
        {
            myTile = other.GetComponent<Floor>();            
        }
    }    
}
