﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DBUtil;
using Sirenix.OdinInspector;

public class GameManager : MonoBehaviour {

    public Vector3[] playerSpawnLocations = new Vector3[] { new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 0, 0) };

    public Quaternion[] playerSpawnRotations = new Quaternion[] { new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, 0, 1), };

    public Floor[] playerFloorSegments;

    [Button]
    public void AddPlayer()
    {
        Debug.Log(playerSpawnLocations);
        Debug.Log(playerSpawnRotations);
        DodgeballUtilities.PlayerConnect(playerPrefab, playerSpawnLocations[players.ToArray().Length], playerSpawnRotations[players.ToArray().Length]);
    }

    public Floor[] playerFloors;
    public Ball ball;
    public GameObject playerPrefab;

    public List<PlayerManager> players = new List<PlayerManager>();

    public int[] playerScores;
        
	// Use this for initialization
	void Start () {
        ball = FindObjectOfType<Ball>();        
    }
	
	// Update is called once per frame
	void Update () {
        //players = FindObjectsOfType<PlayerManager>();
    }
}
