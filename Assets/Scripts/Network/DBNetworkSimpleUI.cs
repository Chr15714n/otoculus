﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DBNetworkSimpleUI : MonoBehaviour
{
	public NetworkManager _manager;

	[SerializeField]
	public int offsetX;

	[SerializeField]
	public int offsetY;

	string serverAddress = "127.0.0.1";

	public enum UINetworkState
	{
		NONE,
		LOBBY,
		GAME
	}
	private UINetworkState _uiNetState;


	void Awake()
	{
		if (PlayerPrefs.GetString("GameServerAddress") != null)
		{
			serverAddress = PlayerPrefs.GetString("GameServerAddress");
		}
	}


	private void OnGUI()
	{
		int xpos = 10 + offsetX;
		int ypos = 40 + offsetY;
		int spacing = 24;

		if(_manager.isNetworkActive)
		{
			return;
		}

		//if (_uiNetState == UINetworkState.NONE)
		{
			if (GUI.Button(new Rect(xpos, ypos, 110, 20), "Host Game(H)"))
			{
				_manager.StartHost();
			}
			ypos += 30;

			if (GUI.Button(new Rect(xpos, ypos, 110, 20), "Join Game(C)"))
			{
				JoinGame(true);
			}
			_manager.networkAddress = serverAddress;
			serverAddress = GUI.TextField(new Rect(xpos + 130, ypos, 100, 20), serverAddress);
			ypos += spacing;
		}

	}

	void HostGame(bool withMocap)
	{
		_manager.StartHost();
	}

	void JoinGame(bool withMocap)
	{
		PlayerPrefs.SetString("GameServerAddress", serverAddress);
		_manager.StartClient();
	}

}
