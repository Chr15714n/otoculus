﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Helper component to place on child triggers for callbacks
/// </summary>
public class DBTriggerComponent : MonoBehaviour
{
	public delegate void NotifyOnTriggerDelegate(DBTriggerComponent triggerComponent, Collider other);

	// Associate these delegates to parent object's callback
	public NotifyOnTriggerDelegate _notifyOnTriggerEnter;
	public NotifyOnTriggerDelegate _notifyOnTriggerStay;
	public NotifyOnTriggerDelegate _notifyOnTriggerExit;

	public GameObject _parentGameObject;


	public void OnTriggerEnter(Collider other)
	{
		// Just forward to delegate
		if(_notifyOnTriggerEnter != null)
		{
			_notifyOnTriggerEnter(this, other);
		}
	}

	public void OnTriggerStay(Collider other)
	{
		// Just forward to delegate
		if (_notifyOnTriggerStay != null)
		{
			_notifyOnTriggerStay(this, other);
		}
	}

	public void OnTriggerExit(Collider other)
	{
		// Just forward to delegate
		if (_notifyOnTriggerExit != null)
		{
			_notifyOnTriggerExit(this, other);
		}
	}
}
