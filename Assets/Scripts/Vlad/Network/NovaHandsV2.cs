﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;

namespace Vlad {
    public class NovaHandsV2 : NetworkBehaviour {
        /*Variable Declaration*/
        [SyncVar]
        public int _uniqueID = 0;

        public Vlad.Networking.NetworkGameManager _netManager;

        public Transform _leftHand, _rightHand;

        [SyncVar]
        public bool _ballControlledLeft = false;

       [SyncVar]
        public bool _ballControlledRight = false;

        [SyncVar]
        public bool _holdingButtonLeft = false;

        [SyncVar]
        public bool _holdingButtonRight = false;


        public NovaBallHeld heldBall;

        public Vector3 LeftVelocity {
            get {
                if (VRDevice.isPresent)
                    return OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
                else
                    return Vector3.Normalize(_leftHand.forward + _leftHand.up) * 5.0f;
            }
        }
        public Vector3 RightVelocity {
            get {
                if (VRDevice.isPresent)
                    return OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
                else
                    return Vector3.Normalize(_rightHand.forward + _rightHand.up) * 5.0f;
            }
        }
        void Start() {
            if (_netManager == null) {
                _netManager = FindObjectOfType<Vlad.Networking.NetworkGameManager>();
            }
        }

        void Update() {

            if (!isLocalPlayer) return;
            if (VRDevice.isPresent) {
                //right hand grab
                if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)) {
                    CmdGrabRight();
                }
                //left hand grab
                if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger)) {
                    CmdGrabLeft();
                }
                //right hand release
                if (OVRInput.GetUp(OVRInput.Button.SecondaryIndexTrigger)) {
                    CmdReleaseRight();
                }
                //left hand release
                if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger)) {
                    CmdReleaseLeft();
                }
            }

            else {
                if (Input.GetMouseButtonDown(0)) {
                    CmdGrabLeft();
                }

                if (Input.GetMouseButtonUp(0)) {
                    CmdReleaseLeft();
                }

                if (Input.GetMouseButtonDown(1)) {
                    CmdGrabRight();
                }

                if (Input.GetMouseButtonUp(1)) {
                    CmdReleaseRight();    
                }
            }

        }//update

        [Command]
        public void CmdGrabLeft() {
            _holdingButtonLeft = true;
            print("Cmd_Grab");
        }

        [Command]
        public void CmdGrabRight() {
            _holdingButtonRight = true;
            print("Cmd_Grab");
        }
        [Command]
        public void CmdReleaseLeft() {
            _holdingButtonLeft = false;
            print("Cmd_Release");
            if (heldBall != null) {
                heldBall.RpcReleaseBall();
            }
        }

        [Command]
        public void CmdReleaseRight() {
            _holdingButtonRight = false;
            print("Cmd_Release");
            if (heldBall != null) {
                heldBall.RpcReleaseBall();
            }
        }
        [Command]
        public void CmdReleaseBall() {

            print("Cmd_Release");

        }



    }///class
}//namespace
