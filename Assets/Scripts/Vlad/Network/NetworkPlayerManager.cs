﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Morgan.Networking;
using UnityEngine.VR;

namespace Vlad
{
    namespace Networking
    {

        public class NetworkPlayerManager : NetworkBehaviour
        {

            public PlayerManager LocalPlayer;
            Transform playerHead;
            Transform leftHand;
            Transform rightHand;
            Transform playerBody;
            public Transform _leftHand;
            public Transform _rightHand;
            public Transform _playerHead;
            public Transform _playerBody;
            internal int _powerLevel;

            // Use this for initialization
            void Start()
            {
                if (isLocalPlayer)
                {
                    LocalPlayer = FindObjectOfType<PlayerManager>();

                    if (VRDevice.isPresent)
                    {
                        playerHead = GameObject.Find("HeadCorrector").transform;
                        leftHand = GameObject.Find("LeftHandCorrector").transform;
                        rightHand = GameObject.Find("RightHandCorrector").transform;
                    }
                    else
                    {
                        playerHead = GameObject.Find("HeadCorrector").transform;
                        leftHand = GameObject.Find("LeftHandCorrector").transform;
                        leftHand.position += new Vector3(-0.25f, 0, 0.25f);
                        rightHand = GameObject.Find("RightHandCorrector").transform;
                        rightHand.position += new Vector3(0.25f, 0, 0.25f);
                    }
                    playerBody = playerHead;
                    

                }
            }

            // Update is called once per frame
            void Update()
            {
                
                if (playerHead != null && _playerHead != null)
                {
                    _playerHead.transform.position = playerHead.position;
                    _playerHead.transform.rotation = playerHead.rotation;
                }
                if (isLocalPlayer && leftHand != null && _leftHand != null)
                {
                    _leftHand.position = leftHand.position;
                    _leftHand.rotation = leftHand.rotation;
                }
                if (isLocalPlayer && rightHand != null && _rightHand != null)
                {
                    _rightHand.position = rightHand.position;
                    _rightHand.rotation = rightHand.rotation;
                }
                //if (isLocalPlayer && playerBody != null && _playerBody != null)
                //{
                //    _playerBody.position = playerBody.position;
                //    _playerBody.eulerAngles = Vector3.zero;
                //}
            }

            public void FixedUpate()
            {

            }

            /**Overridding Network Functions**/

            public override void OnStartLocalPlayer()
            {
                base.OnStartLocalPlayer();
                this.transform.position = new Vector3(0, 0, 0);
            }

            public override void OnStartClient()
            {
                base.OnStartClient();

            }

            public override void OnStartServer()
            {

                base.OnStartServer();


            }

        }

        

    }
}
