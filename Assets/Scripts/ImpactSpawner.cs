﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactSpawner : MonoBehaviour {

    public GameObject environmentImpact;
    public GameObject playerImpact;

    public Vector3 spawnOffset;
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision col)
    {
if (col.gameObject.layer == LayerMask.NameToLayer("EnvCollide"))
        {
            //Debug.Log(col.contacts[0].normal);

            Quaternion rot = Quaternion.FromToRotation(Vector3.up, col.contacts[0].normal);

            SpawnImpact(environmentImpact, col.contacts[0].point + spawnOffset, rot);
            
        }
/*
        if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            SpawnImpact(playerImpact, col.contacts[0].point, Quaternion.identity);
        }
        */
    }


    void SpawnImpact(GameObject go, Vector3 lo, Quaternion ro)
    {

        var impact = Instantiate(go, lo, ro);
        Destroy(impact, 2);

    }

}
