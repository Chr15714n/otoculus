﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBGlyph : MonoBehaviour
{
	public DBGlyphNode[] _glyphNodes;

	public int NodePower { get; set; }
	public int ActiveNodes { get; set; }

	private DBMusicConductor _musicConductor;

	//private bool _glyphActive;
	public bool GlyphActive { get { return gameObject.activeSelf; } }

	private int _beat;

	private DBTeamInfo.TeamID _teamID;
	public DBTeamInfo.TeamID TeamID { get { return _teamID;  } }


	// Use this for initialization
	void Start ()
	{
		_glyphNodes = GetComponentsInChildren<DBGlyphNode>();
		int numGlyphs = _glyphNodes.Length;
		for (int i = 0; i < numGlyphs; ++i)
		{
			_glyphNodes[i].GlyphParent = this;
			_glyphNodes[i].ResetNode();
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (_musicConductor != null && _musicConductor._isPlaying)
		{
			_beat = _musicConductor.currentBeats - 1;
		}
	}

	public void ResetGlyph()
	{
		NodePower = 0;
		ActiveNodes = 0;

		int numGlyphs = _glyphNodes.Length;
		for (int i = 0; i < numGlyphs; ++i)
		{
			_glyphNodes[i].ResetNode();
		}
	}

	public void ActivateGlyph(Vector3 spawnPos, Quaternion spawnRot, DBTeamInfo.TeamID teamID)
	{
		if(gameObject.activeSelf)
		{
			return;
		}

		Debug.Log("Glyph is now active!");

		gameObject.SetActive(true);
		gameObject.transform.position = spawnPos;
		gameObject.transform.rotation = spawnRot;
		_teamID = teamID;

		if (_musicConductor == null)
		{
			// TODO: Music Conductor should be a singleton
			_musicConductor = GameObject.Find("MusicConductor").GetComponent<DBMusicConductor>();
			if (_musicConductor == null)
			{
				Debug.LogWarning("Unable to find MusicConductor in scene!");
			}
		}

		ResetGlyph();
		StartCoroutine("PuzzlePlay");
	}

	public void DeactivateGlyph()
	{
		if (!gameObject.activeSelf)
		{
			return;
		}

		Debug.Log("Glyph is now deactive!");

		StopCoroutine("PuzzlePlay");

		ResetGlyph();

		gameObject.SetActive(false);
	}

	private IEnumerator PuzzlePlay()
	{
		//Debug.Log("PuzzlePlay started!");

		while (true)
		{
			NodePower = 0;
			UpdatePowerNodes();

			yield return _beat == 1;

			UpdatePowerNodes();

			yield return _beat == 2;

			UpdatePowerNodes();

			yield return _beat == 3;

			UpdatePowerNodes();
		}

		//Debug.Log("PuzzlePlay finished");
	}

	private void UpdatePowerNodes()
	{
		int numNodes = _glyphNodes.Length;
		for(int i = 0; i < numNodes; ++i)
		{
			if(_beat == _glyphNodes[i]._nodeID)
			{
				_glyphNodes[i].PowerUp();
			}
			else //if(_glyphNodes[i]._nodeID >= ActiveNodes)
			{
				_glyphNodes[i].PowerDown();
			}
		}
	}

	/// <summary>
	/// Callback when glyph node was triggered
	/// </summary>
	/// <param name="node"></param>
    /// 
    /*
	public void NotifyNodeTriggered(DBGlyphNode node, Collider other)
	{
		DBTriggerComponent triggerComponent = other.GetComponent<DBTriggerComponent>();
		if(triggerComponent != null && triggerComponent._parentGameObject != null)
		{
			DBBall ball = triggerComponent._parentGameObject.GetComponent<DBBall>();
			if(ball._teamID == _teamID && ball._ballState == DBBall.DBBallState.CHARGING)
			{
				if (!node.Triggered)
				{
					node.TriggerIt();

					NodePower++;
					ActiveNodes++;

					if (ActiveNodes == 4)
					{
						ball.SetBallCharged();
					}
				}
			}
		}
	}
    */
}
