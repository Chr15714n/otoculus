﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Vlad {

    public class NovaBall : NetworkBehaviour {
        /*Variable Declaration*/
        private Rigidbody _ballRigidBody;
        public List<Transform> _currentHand = new List<Transform>();//gets list of all hands, but only uses first element
        public NetworkTransform netTransform;
        public int currint = 0;
        public int _playerPower;

        public Transform parHand;

        public int _currPlayer;

        [SyncVar]
        public Vector3 handpos;

        [SyncVar]
        public int _ballPower = 1;

        [SyncVar(hook = "OnGrabbed")]
        public bool beingHeld = false;

        // Use this for initialization
        void Start() {
            _ballRigidBody = GetComponent<Rigidbody>();
            netTransform = GetComponent<NetworkTransform>();
        }

        // Update is called once per frame
        void Update() {
            if (transform.position.y < -5) {
                this.transform.position = new Vector3(0, 5, 0);
                _ballRigidBody.velocity = Vector3.zero;
            }
            if (_currentHand.Count > 0 && !beingHeld) {
                if (isServer) {
                    RpcParentBallToHand();
                }
            }
            if (isServer && beingHeld) {
                // handpos = 
                if (parHand != null) {
                    handpos = parHand.position;
                    transform.position = handpos;
                }
                else {
                    RpcParentBallToHand();
                }
                if (!parHand.GetComponentInParent<NovaHandManager>()._holdingButton) {
                    RpcReleaseBallFromHand();
                }
            }

            if (_ballPower >= 5) {
                _ballPower = 5;
            }

            if (_ballPower <= 1) {
                _ballPower = 1;
            }
        }

        public void OnTriggerEnter(Collider col) {
            if (col.tag.Equals("PlayerHand") && !_currentHand.Contains(col.transform)) {
                _currentHand.Add(col.transform);
                if (GetComponentInParent<NovaHandManager>().UniqueID != _currPlayer) {
                    _playerPower = col.GetComponentInParent<Networking.NetworkPlayerManager>()._powerLevel;
                    _ballPower += _playerPower;
                    _playerPower++;
                    _currPlayer = GetComponentInParent<NovaHandManager>().UniqueID;
                }
            }
            else if (!col.tag.Equals("PlayerHand") && _currPlayer != col.GetComponentInParent<NovaHandManager>().UniqueID) {
                _playerPower = 1;
            }
        }

        public void OnTriggerExit(Collider col) {
            if (col.tag.Equals("PlayerHand")) {
                _currentHand.Remove(col.transform);
            }
        }

        /*custom methods*/

        public void OnGrabbed(bool grabbed) {
            beingHeld = grabbed;
            foreach (var renderer in GetComponentsInChildren<Renderer>()) {
                renderer.enabled = !grabbed;
            }
        }

        [ClientRpc]
        public void RpcParentBallToHand() {
            foreach (Transform _cur in _currentHand) {
                if (_cur.GetComponentInParent<NovaHandManager>()._holdingButton && !beingHeld) {
                    _cur.GetComponentInParent<NovaHandManager>()._ballControlled = true;
                    parHand = _cur;
                    handpos = parHand.position;
                    _ballRigidBody.useGravity = false;
                    _ballRigidBody.velocity = Vector3.zero;
                    beingHeld = true;
                }
            }
        }
        [ClientRpc]
        public void RpcReleaseBallFromHand() {

            if (parHand != null) {
                _ballRigidBody.velocity = ((parHand.GetComponentInParent<NovaHandManager>().throwVelocity) * 5);
                transform.position = parHand.transform.position;
            }
            _ballRigidBody.useGravity = true;
            parHand = null;
            beingHeld = false;
        }
    }//
}//
